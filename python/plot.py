import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import numpy as np
from netCDF4 import Dataset
import os
from matplotlib import animation
import pandas as pd
from matplotlib.colors import LinearSegmentedColormap
from  matplotlib import colors as colors
from matplotlib._png import read_png
import eMAST.path_maker as pm
import eMAST.metadata as md
import eMAST.colorconvention as cc
#import PIL
#import Image
#from PIL import Image,ImageDraw, ImageFilter, ImageFont
#import _imaging
class Map:
	def __init__(self,netCDFObj,varName,mapType,day,month,year, outputDirectory, outputFileName):
	
               
		self.netCDFobj=netCDFObj
		self.mapType = mapType	
	        self.metaData=md.MetaData(varName)	
		self.variableArray = netCDFObj.variables['layer'][:]
		
		#remove any infinite values as this destroys the color pallete
		print('removing infinite')
		self.variableArray[self.variableArray==np.NINF]=float('nan')
		self.variableArray[self.variableArray==np.PINF]=float('nan')
		print(np.max(self.variableArray))
		print(np.min(self.variableArray))
			
		print(self.variableArray.shape)
		print(self.variableArray)
		
		print(self.metaData.getChartUnits())
		self.latArray = netCDFObj.variables['latitude'][:]
		self.lonArray =  netCDFObj.variables['longitude'][:]
		self.varName=varName
		self.day=pm.fixZero(day)
		self.month=pm.fixZero(month)
		self.year=pm.fixDate(year)
		self.outputDirectory=outputDirectory
		self.outputFileName = outputFileName
		self.latMinExtent=np.amin(self.latArray)
		self.latMaxExtent=np.amax(self.latArray)
		self.lonMinExtent=np.amin(self.lonArray)
		self.lonMaxExtent=np.amax(self.lonArray)
		self.plot=""
		self.layer=""
		self.colorMap=""
		self.title=""
                self.date=""
		self.metaData=md.MetaData(varName)
		self.colorConvention =cc.ColorConvention('/g/data/rr9/sjj576/scripts/eMAST/csv/colorConvention.csv',self.varName)

	
	def plotMap(self):			
		self.colorMap=plt.get_cmap(self.colorConvention.colorMap)
		self.plot= Basemap(projection='cyl',llcrnrlat=self.latMinExtent,urcrnrlat=self.latMaxExtent,llcrnrlon=self.lonMinExtent,urcrnrlon=self.lonMaxExtent,resolution='f')
		
		if self.latMinExtent>-45:
			self.plot.readshapefile('/g/data/rr9/sjj576/scripts/eMAST/shapeFiles/australia', 'states')	
		self.plot.drawmapboundary(fill_color='white')
		lon, lat = np.meshgrid(self.lonArray, self.latArray)
		xi, yi = self.plot(lon, lat)
		if self.mapType == 'pcolormesh':
			self.layer=self.plot.pcolormesh(xi,yi,self.variableArray,cmap=self.colorMap,vmin=-self.colorConvention.minValue,vmax=self.colorConvention.maxValue)
		
		if self.mapType == 'contourf':
			self.layer= self.plot.contourf(xi,yi,self.variableArray,cmap=self.colorMap,vmin=-self.colorConvention.minValue,vmax=self.colorConvention.maxValue)	

		print(self.plot)
	

	def writePNGLarge(self):
		outputPath=self.outputDirectory+'pngLarge/'+self.outputFileName+'.png'
                print('Printing PNG Large')
                self.plot.arcgisimage(server='http://server.arcgisonline.com/arcgis/rest/services',service='ESRI_Imagery_World_2D', xpixels = 1500, verbose= True)
		#self.plot.bluemarble()
		#date = str(self.day)+'/'+str(self.month)+'/'+str(self.year)
                #longName=self.metaData.getLongName()
                #units=self.metaData.getChartUnits()
		
		title = '%s%s\n%s' %(longName,units,date)
                self.plot.drawcoastlines() 
                #plt.xlabel(title,fontsize=20)
                  
		colorBar=self.plot.colorbar(self.layer, location='bottom')
                colorBar.set_label(units)
                #plt.figure(frameon=True)
		fig = matplotlib.pyplot.gcf()
                fig.set_size_inches(24,12)
                if not os.path.exists(self.outputDirectory+'/pngLarge/'):
                       os.makedirs(self.outputDirectory+'/pngLarge')
                plt.savefig(outputPath,bbox_inches='tight')

	def writePNGMedium(self):
                # create output path for png medium
		outputPath=self.outputDirectory+'pngMedium/'+self.outputFileName+'.png'
                
		
		print('Printing PNG Medium')
                logo = read_png('/home/576/sjj576/bitbucket/eMAST/python/eMAST/images/example.png')
                plt.figimage(logo,yo=370)
                # getting title
                
 		date = str(self.day)+'/'+str(self.month)+'/'+str(self.year)
                longName=self.metaData.getLongName()
                units=self.metaData.getChartUnits()
		#plt.pcolor(X, Y, v, cmap=cm)
		#plt.clim(-4,4)
                title = '%s%s\n%s' %(longName,units,date)
                self.plot.drawcoastlines(linewidth=0.5)
                #plt.xlim(0, 20)
		#plt.ylim(0, 20)
		plt.xlabel(title,fontsize=10)
                
		colorBar=self.plot.colorbar(self.layer, location='right')
                #colorBar.set_label(units)
                self.layer.set_clim(vmin=0,vmax=2)
		#plt.figure(figsize=(12,6))
                fig = matplotlib.pyplot.gcf()
                fig.set_size_inches(7.29,4.6875)
		#plt.subplots_adjust(left=0.1, right=0.9, top=0.1)
		if not os.path.exists(self.outputDirectory+'/pngMedium/'):
                       os.makedirs(self.outputDirectory+'/pngMedium')
                #plt.savefig(outputPath)
		plt.savefig(outputPath)


	def writePDF(self):
		outputPath=self.outputDirectory+'pdf/'+self.outputFileName+'.pdf'
		print('Printing PDF')
                date = str(self.day)+'/'+str(self.month)+'/'+str(self.year)
                longName=self.metaData.getLongName()
                title = longName+'\n'+date
                units=self.metaData.getUnits()
                plt.title(title)
                colorBar=self.plot.colorbar(self.layer, location='bottom')
                colorBar.set_label(units)
                #plt.figure(frameon=True)
                if not os.path.exists(self.outputDirectory+'/pdf/'):
                       os.makedirs(self.outputDirectory+'/pdf')
                plt.savefig(outputPath,format='pdf')
	
	def writePNGSmall(self):
		outputPath=self.outputDirectory+'/pngSmall/'+self.outputFileName+'.png'
                self.plot.drawcoastlines()
		print('Printing Animation')
                #plt.figure(frameon=False)
                if not os.path.exists(self.outputDirectory+'/pngSmall/'):
                       os.makedirs(self.outputDirectory+'/pngSmall')
                fig = matplotlib.pyplot.gcf()
		fig.set_size_inches(1.47,1.47)
		plt.savefig(outputPath,bbox_inches='tight', pad_inches=0)
		
	
class Writer:
	def __init__(self,startYear,endYear,varName):
		self.startYear=startYear
		self.endYear=endYear
		self.varName=varName
	        self.filePath =pm.FilePath(self.varName)
	
	def write(self,imageType):
		years = range(self.startYear,self.endYear)
		if self.filePath.frequency=='mon':
			monthsToProcess=12

		if self.filePath.frequency=='yr':
			monthsToProcess=1

		months = range(1,monthsToProcess+1)
		day =1
		for year in years:
			for month in months:
				inputPath=self.filePath.getInputDirectoryStructure(year,month,day)
				#inputPath='%s/final'%(inputPath)
				print('Input Path: '+inputPath)
				outputDirectoryStructure=self.filePath.getVisualisationDirectoryStructure()
				outputFileName = self.filePath.getOutputFileName(year,month,day)
				# get input path and output directory
				print('Output directory Structure: '+outputDirectoryStructure)
				print('Output file name: '+outputFileName)
			        #outputFileName = pm.netCDFSuffix(outputFileName)	
				#open netcdf file from input directory
				netCDFObj = Dataset(inputPath)
				print(netCDFObj)
						
				# create map object
				map = Map(netCDFObj,self.varName,'contourf',day,month,year,outputDirectoryStructure,outputFileName)
				netCDFObj.close()
				
				#plot map
				map.plotMap()
				
				# write output based on plotType
				if imageType == 'pngMedium':
					print('Writing PNG')
					map.writePNGMedium()
					
				if imageType == 'pdf':
					print ('Writing  PDF')
					map.writePDF()
				
				if imageType =='pngSmall':
					print('Printing Animations')
					map.writePNGSmall()
				
			       	
				if imageType =='pngLarge':
					print('Printing Large PNG')
					map.writePNGLarge()
#

writer = Writer(1979,1980,'eMAST_mon_alph_v1m1_1970_2012')
writer.write('pngMedium')

#eMAST_mon_tmax_
#eMAST_mon_parr_v1m1_1970_2012
#eMAST_mon_etpo_v1m1_1970_2012
#eMAST_mon_runo_v1m1_1970_2012
#eMAST_mon_gd10_v1m1_1970_2012
#eMAST_mon_gd05_v1m1_1970_2012
#eMAST_mon_gd00_v1m1_1970_2012
#eMAST_mon_eteq_v1m1_1970_2012
#eMAST_mon_etde_v1m1_1970_2012
#eMAST_mon_moin_v1m1_1970_2012
#eMAST_mon_chil_v1m1_1970_2012
#eMAST_mon_etac_v1m1_1970_2012
#eMAST_mon_parr_v1m1_1970_2012
