import splash as sp
import netCDFTools as ncdf
import path_maker as pm

class Model_Engine:
	def __init__(self,modelInputs, startYear,endYear))
		
		self.modelInputs = modelInputs
		self.startYear = startYear
		self.endYear = endYear

	
	def runSplash():
		# create staging directories for eteq, etpo adn etac
		pm.createStagingDirectory('eMAST_eWATCH_day_eteq_v1m1_1979_2012')
		pm.createStagingDirectory('eMAST_eWATCH_day_etpo_v1m1_1979_2012')
		pm.createStagingDirectory('eMAST_eWATCH_day_etac_v1m1_1979_2012')

		
		# get elevation from elevation constant	
		elevationArray = netcdf.getVariableArray(modelInput.constantPathList[0],'elevation')
		

		# get latitude and longitude from constant

		lats=np.array(sradNetCDF.variables['latitude'])
		lons=np.array(sradNetCDF.variables['longitude'])
		
		landClip = land_clip(elevationArray)
		# iterate through each time step

		# Set 'ocean' elevations equal to zero:
		elevationArray=sp.oceanElevation(elevation_array,landClip)
		


		# Initialize monthly key outputs:
		self.ppfd_mo = sp.initialise_ppfd()  # PPFD, mol/m^2
		self.aet_mo = sp.initialise_aet_mo()   # AET, mm
		self.eet_mo = sp.initialise_eet_mo()# EET, mm
		self.pet_mo = sp.initialise_pet_mo()   # PET, mm

		# Annual total top-of-atmosphere PPFD, Qo_ann
		self.qo_ann = sp.initialise_qo_ann()

		# Initialize soil moisture:
		# * w.shape: days, lat, lon
		self.w = initialise_w(self.startYear,elevationArray,landClip,
		
		#begin iterating through the days
		
		# count how many model inputs there are
		
		variableArray={}
		
		fmt = '%Y.%m.%d'
		dt = datetime.datetime.strptime(s, fmt)
                tt = dt.timetuple()
        	dayOfYear=(tt.tm_yday)
		# get array of string of all through years,months and days
		
		



		# daily time steps
		timeSteps=pm.getYearMonthDayArray(1981,1982)
		for timeStep in range(0,timeSteps):
			#0 is srad
			
			self.variableArray['srad'] = ncdf.getVariableArray(self.modelInputs[0][timeStep],'surface_downwelling_shortwave_flux_in_air')
			 self.variableArray['srad']= sp.sunshineHours(variableArray['srad',lats)

			
			
			#1 is prec
			self.variableArray['prec']=netcdf.getVariableArray(self.modelInputs[1][timeStep],'convective_precipitation_flux')

			#2 is tair
			self.variableArray['tair']=netcdf.getself.modelInputs[2][timeSteps],'air_temperature')
			
			
			#calculate evaporative supply rate
			self.variableArray['evaporativeSupplyRate'] = evaporativeSupplyRate(self.w)
			
			#get day of the year from string
			day = pm.findDayOfYear(timeStep)
                	
			my_evap = sp.EVAP_G(day,elevationArray,variableArray['srad'],variableArray['prec'],variableArray['evaporativeSupplyRate'],year)
			
			# update soil moisture
			self.w = update_soil_moisture(my_evap,self.w)
			
			
		 	#saveFiles


	


		
