import numpy as np
import pandas as pd
from pylab import *
import matplotlib.colors as col
from matplotlib.colors import LinearSegmentedColormap


class ColorConvention:
        def __init__(self,colorConventionSource,varName):
                self.colorConventionSource = colorConventionSource
                self.colorConvention=pd.DataFrame.from_csv(self.colorConventionSource)
                self.varName= varName
                self.colorConventionSeries= self.colorConvention.loc[self.varName]
                self.colorConventionDictionary = self.colorConventionSeries.to_dict()
                self.colorMap = self.colorConventionDictionary['color'].decode('utf-8','ignore')
		self.minValue = self.colorConventionDictionary['minValue']
	 	self.maxValue = self.colorConventionDictionary['maxValue']

		
hexDict={}
hexDict['gppb']=['#FEE391','#FFF7BC','#FFFFE5','#F7FCB9',
		'#D9F0A3','#ADDD8E','#41AB5D','#238B45',
		'#1E5085','#3477BF','#0978ED']

hexDict['gpp']=['#FEE391','#FFF7BC','#FFFFE5','#F7FCB9',
                '#D9F0A3','#ADDD8E','#41AB5D','#238B45',
                '#006D2C','#00441B','#006837']

hexDict['prec']=["#FEFEFE","#FEBE58","#FEAC00","#FEFE00",
		"#B2FE00","#4CFE00","#00E499","#00A4FE",
		"#3E3EFE","#B200FE","#FE00FE","#FE4C9B"]

hexDict['temp']=["#990099","#FE00FE","#FBAFFB","#CBCBCC",
		"#6565FE","#33CBFE","#9AFEFE","#00CB00",
		"#65FE65","#FEFECB","#FEFE33","#FECB65",
		"#FECBCB","#FE9999","#FE3333","#CB0000",
		"#986532","#993300"]

hexDict['rad']=["#CBCBCC","#6565FE","#33CBFE","#9AFEFE",
		"#00CB00","#65FE65","#CBFECB","#FEFECB",
		"#FEFE33","#FECB65","#FECBCB","#FE9999",
		"#FE3333"]

#hexDict['greenToBrown'] = ['#238B45','#1E5085','#3477BF','#0978ED']

#hexDict['gpp']=['#FEE391','#F7FCB9','#006837']


def makeColorMapFromhex(hexDict):
	

	colorConverter = col.ColorConverter()	

	# create outer list of N x 3 dim

	for key,colors in hexDict.iteritems():
	   		
		listRedOuter=[]
        	listGreenOuter=[]
        	listBlueOuter=[]
		numberOfColors = float(len(colors))
        	print('Number of colors %s')%(numberOfColors)
		
        	interval = round(1/numberOfColors,4)
		print('Interval : %s')%(interval)
		count = 1
		for color in colors:
			
			print('Color Number : %s') %(count)
			print ('Key:  %s')% (key)
			print('Color : %s') %(color)
			 
			beg = count *interval
			print('Beggining : %s') %(beg)

			end = (count +1) * interval
			print ('End : %s') %(end)
			
			RGB = colorConverter.to_rgba(color)	
			#print ('RGB : %s') %(RGB) 
			print RGB
			if count == 1:
				print('First Tuples')
				listRed = ((0),(RGB[0]),(RGB[0]))
				listGreen =((0),(RGB[1]),(RGB[1]))
				listBlue = ((0),(RGB[2]),(RGB[2]))		
				print listRed

			if count >=2 and count < numberOfColors:
				listRed = ((beg),(RGB[0]),(RGB[0]))
				listGreen =((beg),(RGB[1]),(RGB[1]))
				listBlue = ((beg),(RGB[2]),(RGB[2]))
				print(listRed)
		
			if count  == numberOfColors:
				print('Final Color')
				listRed = ((1),(RGB[0]),(RGB[0]))
				listGreen =((1),(RGB[1]),(RGB[1]))
				listBlue =((1),(RGB[2]),(RGB[2]))
				print(listRed)
			#print listRedOuter
			#print listGreenOuter
			#print listBlueOuter
			
			listRedOuter.append(listRed)
			listGreenOuter.append(listGreen)
			listBlueOuter.append(listBlue)
			count = count +1
		
		tupleRed = tuple(listRedOuter)
		print('Tuple red')	
		print(tupleRed)
		print(type(tupleRed))
		
		
		tupleGreen = tuple(listGreenOuter)
		print(tupleGreen)
		tupleBlue = tuple(listBlueOuter)
		print(tupleBlue)
	
		colorDictionary = {'red': tupleRed, 'green': tupleGreen, 'blue':tupleBlue} 
		#print colorDictionary['red']
		my_cmap = matplotlib.colors.LinearSegmentedColormap(key,colorDictionary,256,gamma=0.75)
		cm.register_cmap(name=key, cmap=my_cmap)
	
		
def bluegreen(y):
    red = [(0.0, 0.0, 0.0), (0.5, y, y), (1.0, 0.0, 0.0)]
    green = [(0.0, 0.0, 0.0), (0.5, y, y), (1.0, y, y)]
    blue = [(0.0, y, y), (0.5, y, y),(1.0,0.0,0.0)]
    colordict = dict(red=red, green=green, blue=blue)
    bluegreenmap = LinearSegmentedColormap('bluegreen', colordict, 256)
    cm.register_cmap(name='bluegreen',cmap=bluegreenmap)
    #return bluegreenmap



bluegreen(0.7)


makeColorMapFromhex(hexDict)		





