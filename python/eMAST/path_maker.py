import numpy as np
import pandas as pd
import os.path
import os
import shutil
import datetime
filePathSource = os.path.join(os.path.dirname(__file__), 'csv/namingConvention.csv')


def determineRightFilePathCSV(variable):
                rightFilePathSource =""
                #for filePathSource in filePathSources:
                
		# open data frame
		testFilePathCSV= pd.DataFrame.from_csv(filePathSource)
		print('Testing %s ') %(filePathSource)
		# try to find variable
		
		try:
			dataProductSeries = testFilePathCSV.loc[variable, :]
			rightFilePathSource = filePathSource
			print('The right filePath was located in file %s') %(rightFilePathSource)

		except:
			print('%s is not in %s') % (variable, filePathSource)
                
                return rightFilePathSource



class FilePath:
	def determineRightFilePathCSV(variable):
        	rightFilePath =""
		for filePathSource in filePathSources:
			# open data frame
			testFilePathCSV= pd.DataFrame.from_csv(filePathSource)
			# try to find variable
			try:
				dataProductSeries= testFilePathCSV.loc[variable, :]
				rightFilePathSource = testFilePathCSV
			except:
				print('%s is not in %s') % (variable,testFilePathCSV)
		print('The right metadata was located in file %s') %(rightFilePathSource)
		return rightFilePathSource

	def __init__(self,varName):
                self.namingConventionSource=determineRightFilePathCSV(varName)
                print(self.namingConventionSource)
		self.namingConvention=pd.DataFrame.from_csv(self.namingConventionSource)
                self.varName = varName
                self.dataProductSeries= self.namingConvention.loc[self.varName, :]
                self.variableDictionary=self.dataProductSeries.to_dict()
                self.staging = self.variableDictionary['staging'].decode('utf-8','ignore')
                self.field = self.variableDictionary['field'].decode('utf-8','ignore')
                self.activity = self.variableDictionary['activity'].decode('utf-8','ignore')
                self.institution = self.variableDictionary['institution'].decode('utf-8','ignore')
                self.model = self.variableDictionary['model'].decode('utf-8','ignore')
                self.resolution = self.variableDictionary['resolution'].decode('utf-8','ignore')
                self.version = self.variableDictionary['version'].decode('utf-8','ignore')
                self.frequency = self.variableDictionary['frequency'].decode('utf-8','ignore')
                self.realm = self.variableDictionary['realm'].decode('utf-8','ignore')
                self.variable = self.variableDictionary['variable_name'].decode('utf-8','ignore')
                self.ensemble = self.variableDictionary['ensemble'].decode('utf-8','ignore')
                self.timeSeries = self.variableDictionary['time_series'].decode('utf-8','ignore')

 	def getInputDirectoryStructure(self,year,month,day):
                day=fixZero(day)
                year = fixDate(year)
                month = fixZero(month)
                
		print('Getting Input File')
                if self.frequency == 'yr':
                        month,day=""
                if self.frequency =='mon':
                        day=""
                
		inputFileStructure = '%s_%s_%s_%s_%s_%s%s.nc' % (self.model,self.frequency,self.variable,self.version,year,month,day)
		inputDirectoryStructure = '%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/' % (self.staging,self.field,self.activity,
							       self.institution,self.model,self.resolution,
							       self.version,self.frequency,self.realm,
								self.variable,self.ensemble,self.timeSeries) 
		completeInputPath = '%s%s' % (inputDirectoryStructure,inputFileStructure)
		

		if os.path.isfile(completeInputPath) == False:
			inputFileStructure = '%s_%s_%s_%s_%s_%s%s%%s.nc' % (self.model,self.frequency,self.variable,'v1m0',year,month,day)	  	
			completeInputPath = '%s%s' % (inputDirectoryStructure,inputFileStructure)
		
		if os.path.isfile(completeInputPath) == False:
			inputFileStructure = '%s_%s_%s_%s_%s_%s%s%s.nc' % (self.institution,self.model,self.frequency,self.variable,self.version,year,month,day)	
			completeInputPath = '%s%s' % (inputDirectoryStructure,inputFileStructure)
		
		if os.path.isfile(completeInputPath) == False:
                        inputFileStructure = '%s_%s_%s_%s_%s_%s%s%s.nc' % (self.institution,self.model,self.frequency,self.variable,'v1m0',year,month,day)
                        completeInputPath = '%s%s' % (inputDirectoryStructure,inputFileStructure)
		if os.path.isfile(completeInputPath) == False:
                        inputFileStructure = '%s_%s_%s_%s_%s%s%s.nc' % (self.model,self.frequency,self.variable,self.version,year,month,day)
                        completeInputPath = '%s%s' % (inputDirectoryStructure,inputFileStructure)


		return completeInputPath


 	def getOutputDirectoryStructure(self):
                print('Getting Output Directory')
                inputDirectoryStructure = '/g/data/rr9/sjj576/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/'%(self.field,self.activity,self.institution,
												self.model,self.resolution,self.version,
												self.frequency,self.realm,self.variable,
												self.ensemble,self.timeSeries)
                return inputDirectoryStructure

        def getOutputFileName(self,year,month,day):
                day=fixZero(day)
                year = fixDate(year)
                month = fixZero(month)
		print('Getting file name')
                if self.frequency == 'yr':
                        month,day=""
                if self.frequency =='mon':
                        day=""


                fileName=self.model+'_'+self.frequency+'_'+self.variable+ '_'+self.version+'_'+year +month + day
                return fileName


	def getVisualisationDirectoryStructure(self):
		print('Getting Visualisation Output Directory')
                inputDirectoryStructure = '/g/data/rr9/sjj576/visualisations'+'/'+self.field+'/'+self.activity+'/'+self.institution+'/'+self.model+'/'+self.resolution+'/'+self.version+'/'+self.frequency+'/'+'/'+self.realm+'/'+self.variable +'/'+self.ensemble+'/'+self.timeSeries+'/'
		return inputDirectoryStructure

def fixZero(month):
	print('Converting date')
	if month <10:
		month = '0'+str(month)
	month=str(month)
	return(month)

def fixDate(date):
	date=str(date)
	return(date)

def checkAndCreatePath(outputPath):
	if not os.path.exists(outputPath):
                       os.makedirs(outputPath)

def netCDFSuffix(fileName):
	fileName = fileName+'.nc'
	return fileName

# method for creating directory based on list of products
def namingConventionStagingDirectory(variable):
	print ('Creating directory')
	filePath = FilePath(variable)
	inputDirectoryStructure = '%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/'%(filePath.staging,filePath.field,filePath.activity,filePath.institution,
                                                                                                filePath.model,filePath.resolution,filePath.version,
                                                                                                filePath.frequency,filePath.realm,filePath.variable,
                                                                                                filePath.ensemble,filePath.timeSeries)
	return (inputDirectoryStructure)

#create staging directory
def createStagingDirectory(variable):
	outputPath = namingConventionStagingDirectory(variable)

	if os.path.exists(outputPath):
		print ('Staging output path %s already exists') % (outputPath)
	if not os.path.exists(outputPath):
		print('Creating staging output path %s') % (outputPath)
		os.makedirs(outputPath)
		
def createMultipleStagingDirectories(variableList):
	for variable in variableList:
		createStagingDirectory(variable)


def moveDirectoryContentsToStaging(sourceDirectory,variable):
	print('Moving contents of directory to other directory')
	source = os.listdir(sourceDirectory)
	destination = namingConventionStagingDirectory(variable)
	for file in source:
        	if file.endswith('.nc'):
			completePath = '%s/%s'%(sourceDirectory,file)
			print('Copying source %s to target directory %s')%(completePath,destination)
			
			shutil.copy(completePath,destination)

# method for returning the files inside a directory
def getFileNameList(directory):
	print('Listing files inside %s') % directory
	files = os.listdir(directory)
	print files
	return files


def checkAndCreateDirectory(outputPath):
	if os.path.exists(outputPath):
                print (' output path %s already exists') % (outputPath)
        if not os.path.exists(outputPath):
                print('output path %s') % (outputPath)
                os.makedirs(outputPath)


def getYearMonthDayArray(startYear,endYear):
	yearMonthDayList=[]
	yearMonthDayDict={}
	years = range(startYear,endYear+1)
	months = range(1,13)
	daysInMonth=""
	for year in years:
                
                for month in months:
                        if  month ==1 or month == 3 or month == 5 or month == 7 or month ==8 or month ==10 or month ==12:
                                daysInMonth = 30
                        if month ==4 or month ==6 or month ==9 or month ==11:
                                daysInMonth =29
                        if month ==2:
                                daysInMonth=27
                        days = range(1,daysInMonth+1)
            
                        for day in days:
				yearMonthDayDict = {'year':year,'month':month,'day':day}
				print(yearMonthDayDict)
				yearMonthDayList.append(yearMonthDayDict)
	
	return (yearMonthDayList)

#def findDayOfYear(timeStepDict):
#	s = '%s.%s.%s'% (str(timeStepDict['year']),pm.fixZero(timeStepDict['month']),pm.fixZero(timeStepDict['day'])
#	dt = datetime.datetime.strptime(s, fmt)
#        tt = dt.timetuple()
#        dayOfYear=(tt.tm_yday)
#	return dayOfYear
