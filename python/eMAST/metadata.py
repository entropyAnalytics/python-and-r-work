import path_maker as pm
import numpy as np
import pandas as pd
import netCDFTools as netcdf
import netCDF4
from netCDF4 import Dataset
from osgeo import gdal, osr

import os
source1 = os.path.join(os.path.dirname(__file__), 'csv/metadata.csv')
source2= os.path.join(os.path.dirname(__file__), 'csv/ewatch_metadata.csv')
metaDataSources=[source1,source2]

class MetaData:
        def __init__(self,variable):
                
		def determineRightMetaData(variable):
			rightMetaDataSource =""
			for metaDataSource in metaDataSources:
				
                		# open data frame
				testMetaData= pd.DataFrame.from_csv(metaDataSource)			
				
				# try to find variable
				try:
					dataProductSeries= testMetaData.loc[variable, :]
					rightMetaDataSource = metaDataSource
				except:
					print('%s is not in %s') % (variable,metaDataSource)
			print('The right metadata was located in file %s') %(rightMetaDataSource)
			return rightMetaDataSource

		self.metaDataSource=determineRightMetaData(variable)
                self.metaData= pd.DataFrame.from_csv(self.metaDataSource)
                self.variable = variable
                self.dataProductSeries= self.metaData.loc[variable, :]
                self.variableDictionary=self.dataProductSeries.to_dict()
		
        
        def getLongName(self): 
                print('Getting Long Name')
                longName = self.variableDictionary['long_name'].decode('utf-8','ignore')
                longName= longName.title()
                return(longName)
        
        def getUnits(self):
                print('Getting Units')
                units = self.variableDictionary['units'].decode('utf-8','ignore')
                if units=='nounits' or units=='(no?units)':
                        units=""
   		if 'no' in units or'units' in units:
			units=""
		return(units)

	def getChartUnits(self):
		print('Getting Units for the chars')
                units = self.variableDictionary['units'].decode('utf-8','ignore')
                if units=='nounits' or units=='(no?units)':
                        units=""
		else:
			units = '(%s)'%(units)
                return(units)

	def getStandardName(self):
		print('Getting Standard Namee')
		standardName = self.variableDictionary['standard_name'].decode('utf-8','ignore')
		return(standardName)

	def writeMetaData(self):
		
		# find the file path of the based on the variable name
		directory = pm.namingConventionStagingDirectory(self.variable)
		
		# final directory
		
		finalDirectory = '%s/final/' % (directory)
		print('Directory containing files to write meta data %s') %(directory)		

		# get a list of all the directories in path
		files = pm.getFileNameList(directory)
		metaData = self.variableDictionary
	        print(metaData)	
		# loop through the files in the path, make complete path, 
		for file in files:
			completePath = '%s%s'%(directory,file)
			print('Updating meta data for %s: ')%(completePath)
			
			#open existing netcdf file
			ncdfObj = netcdf.ncopen(completePath)
			
			# get longitude
			lon = netcdf.getLongitudeArray(completePath)
			# get latitude
			lat = netcdf.getLatitudeArray(completePath)
		        finalPath = '%s%s' %(finalDirectory,file)	
			# get data
			data = netcdf.getVariableArray(completePath,'layer')
			
	
			#create new file staging directory for new file
			pm.checkAndCreateDirectory(finalDirectory)	
			
			
			netcdf.make_nc(outfile=finalPath,
					varname=self.getStandardName(),
					data= data,
					lati=lat,
					loni = lon,
					metaData=self.variableDictionary)
			print(netcdf)
				
			#updateMetaData(completePath, metaData, netCDFObj)

		

def updateMetaData(completePath, productMetaData,netCDFObj):
        print('Updating Metadata')
        print(completePath)
        print(productMetaData)
       
        for columnKey, columnValue in productMetaData.iteritems():
                columnKey=str(columnKey)
                columnKey = columnKey.decode('utf-8','ignore')

                columnValue=str(columnValue)
                columnValue = columnValue.decode('utf-8','ignore')
        
                writeMetaData=True
                if writeMetaData ==True:
                        setattr(netCDFObj,columnKey,columnValue)
                for name in netCDFObj.ncattrs():
                        print 'Global attr', name, '=', getattr(netCDFObj,name)
        netCDFObj.close()



