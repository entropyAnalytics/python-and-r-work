import netCDF4
from netCDF4 import Dataset
from osgeo import gdal, osr
from datetime import datetime
import os
import numpy as np
import path_maker as pm

def ncopen(path, permission='r', format='NETCDF4_CLASSIC'):
    """
    Return a netCDF object.
    Default permission is 'a' for appending.
    """
    print('Opening NetCDF FIle: '+ path)
    if permission == 'w':
         print('Opening write netCDF')
         netCDFObj = Dataset(path,mode=permission,format=format)
    if permission =='r':
        print('Opening read netCDF')
        netCDFObj = Dataset(path,mode=permission,format=format)
    if permission =='a':
	print('Opening append netCDF')
        netCDFObj = Dataset(path,mode=permission)
    print(netCDFObj)
    
    for dimension in netCDFObj.dimensions.values():
        print(dimension)
    print(netCDFObj.variables)
    #for name in netCDFObj.ncattrs():
    #   print 'Global attr', name, '=', getattr(netCDFObj,name)   

    return netCDFObj

def getVariableArray(netCDFPath,variableName):
	netCDFObj = ncopen(netCDFPath)
	array = np.array(netCDFObj.variables[variableName])
	#array=array[0]
	netCDFObj.close()
	array = array
	print(array.shape)
	print(array)
	return (array)



def getLatitudeArray(netCDFPath,variableName='latitude'):
        netCDFObj = ncopen(netCDFPath)
        array = np.array(netCDFObj.variables[variableName])
        netCDFObj.close()
       
        print(array.shape)
        print(array)
        return (array)


def getLongitudeArray(netCDFPath,variableName='longitude'):
        netCDFObj = ncopen(netCDFPath)
        array = np.array(netCDFObj.variables[variableName])
        netCDFObj.close()

        print(array.shape)
        print(array)
        return (array)

def getElevationArray(netCDFPath,variableName):
        netCDFObj = ncopen(netCDFPath)
        array = np.array(netCDFObj.variables[variableName])
        netCDFObj.close()

        print(array.shape)
        print(array)
        return (array)

def removeNAS(d):
	d[d==np.nan]=-9999.
	d[d>=float('inf')]=-9999.
	d[d<=float('-inf')]=-9999.
	d[d<=-3.4e+38]=-9999.
	d[d<=-3.4e+1]=-9999.
	d[d<0]=-9999.
	return(d)


def saveNetCDF(data,identifier,cur_date,cur_day,lonArray,latArray):
        filePath = pm.FilePath(identifier)
        outputDirectory = filePath.getOutputDirectoryStructure()
        outputFileName = filePath.getOutputFileName(cur_date.year,cur_date.month,cur_day+1)+'.nc'
        completePath = '%s%s'%(outputDirectory,outputFileName)
     

        make_netCDF4CF16_daily(data=data, year=str(cur_date.year),month=pm.fixZero(cur_date.month), \
                                     day=None,  \
                                     metadata=None, \
                                     outdirectory=outputDirectory, \
                                     outfilename=outputFileName, \
                                     header=getHeader(lonArray,latArray), \
                                     lats=latArray, \
                                     lons=lonArray, \
                                     productname=identifier)

#def replaceMissingValuesWithZeros(data,elevationData):
	


def  make_netCDF4CF16_daily  (data=None, \
                             year=None, \
                             month=None, \
                             day=None,  \
                             metadata=None, \
                             outdirectory=None, \
                             outfilename=None, \
                             header=None, \
                             lats=None, \
                             lons=None, \
                             productname=None):
    """
    Makes a netCDF CF file from;
    
    :data:
    A numpy array of the data to be converted
    
    :year:
    Year of the dataset
    
    :month: 
    Month of the dataset
    
    :day:
    Day of the dataset


  :metadata: 
    A pandas data frame containing the metadata
    
    :outfilename:
    An output filename
    
    :proj4:
    PROJ.4 description
    http://nsidc.org/data/atlas/epsg_4326.html
     
    """


    # Create the datetime for the observation, assume the first day of every month
    #obs_date = datetime.date(int(year), int(month))
    #obs_date = datetime.datetime.combine(obs_date, datetime.datetime.min.time())

    # Setup the output directory
    out_dir = os.path.join(outdirectory)
    if (not os.path.exists(out_dir)):
        os.makedirs(out_dir)

    # Output filename
    outfname = os.path.join(out_dir, outfilename)
   
    print(outfname)
    # Create the output dataset
    outds = ncopen(outfname, permission='w')
    #set_timelatlon(outds, 1, header['lines'], header['samples'], timeunit='seconds since 1970-01-01 00:00:00')
    #add_data(outds, 'latitude', lats)

    lat   = outds.createDimension('latitude', len(lats))
    lon   = outds.createDimension('longitude', len(lons))
 #times = ncds.createVariable('time','f8',('time',))
    latitudes = outds.createVariable('latitude','f4',('latitude',))
    longitudes = outds.createVariable('longitude','f4',('longitude',))
    #variable = ncds.createVariable(varname,'f8',('time','latitude','longitude',))
    latitudes[:] = lats[:]
    longitudes[:] = lons[:]
    #times[:] = timei[:]





#add_data(outds, 'longitude', lons)

    #add_time(outds, [obs_date])
    nodata=-9999
    if nodata != -9999:
        data[data == nodata] = -9999
    nodata = -9999
    #dtype='f4'
    #set_variable(outds, variable_name, dtype=dtypeMapping(band.DataType), fill=nodata)
    #set_variable(outds, productname, dtype=dtypeMapping(d.dtype), fill=nodata)
    variable = outds.createVariable(productname,'f4',('latitude','longitude',), fill_value=nodata)
    variable[:] = data

#    add_data(outds,productname, data)
    #set_attributes(outds, meta2)
    #
    #print outds
    crs_meta = {}
    ref = osr.SpatialReference()
    ref.ImportFromEPSG(header['epsg'])
   # set_variable(outds, 'crs', dtypeMapping('f4'), dims=())
    #set_variable(outds, 'crs', dims=())
    new_key = 'crs:name'
    crs_meta[new_key] = ref.GetAttrValue('geogcs')
    new_key = 'crs:datum'
    crs_meta[new_key] = ref.GetAttrValue('datum')
    new_key = 'crs:longitude_of_prime_meridian'
    crs_meta[new_key] = 0.0
    new_key = 'crs:inverse_flattening'
    crs_meta[new_key] = ref.GetInvFlattening()
    new_key = 'crs:semi_major_axis'
    crs_meta[new_key] = ref.GetSemiMajor()
    new_key = 'crs:semi_minor_axis'
    crs_meta[new_key] = ref.GetSemiMinor()
    #
    # TODO : Make this work - to "set" 
    #
    #
    # print crs_meta
    #{'crs:datum': None, 
    #'crs:semi_minor_axis': 6356752.314245179, 
    #'crs:semi_major_axis': 6378137.0, 
    #'crs:name': None, 
    #'crs:longitude_of_prime_meridian': 0.0, 
    #'crs:inverse_flattening': 298.257223563}
    # Write the CRS metadata
    #set_attributes(outds, crs_meta)

    # Write the rest of the global metadata
    #set_attributes(outds, meta3)
  # Write the rest of the global metadata
    #set_attributes(outds, meta3)

     # Get the extents of the dataset
    #extents = getMinMaxExtents(samples, lines, geoT)

    # Set the extents metadata
    outds.geospatial_lat_min = header['latsmin']
    outds.geospatial_lat_max = header['latsmax']
    outds.geospatial_lat_units = 'degrees_north'
    outds.geospatial_lat_resolution = header['resolution']
    outds.geospatial_lon_min = header['lonsmin']
    outds.geospatial_lon_max = header['lonsmax']
    outds.geospatial_lon_units = 'degrees_east'
    outds.geospatial_lon_resolution = header['resolution']
    #
    ncclose(outds)


def getHeader(lons,lats):
	 x, y = (lons, lats)
         header = {'bands': 1, \
		  'latsmin': min(lats), \
		  'lonsmin':min(lons), \
		  'latsmax': max(lats), \
		  'lonsmax':max(lons), \
		  'epsg':4326, \
		  'resolution':0.5}	
	 return header

def ncclose(ncobj):
    """
    Close a netCDF object.
    This is required to write the final state of the netCDF object to disk.
    """
    ncobj.close()


def make_nc(outfile, varname, data, lati, loni, metaData):
    print(data)
    header = getHeader(loni,lati)
    nodata=-9999   
     
    #remove missing data and replace with -9999
    data = removeNAS(data)

    #remove infinite date and replace with -9999
    
   
    #create new netcdffile
    print('Outfile is %s') %(outfile) 
    ncds = Dataset(outfile, 'w', format='NETCDF4_CLASSIC', zlib=True)
   # t  = ncds.createDimension('time', None)
    lat   = ncds.createDimension('latitude', len(lati))
    lon   = ncds.createDimension('longitude', len(loni))

    #times = ncds.createVariable('time','f8',('time',))
    latitudes = ncds.createVariable('latitude','f4',('latitude',))
    longitudes = ncds.createVariable('longitude','f4',('longitude',))
    
    #variable = ncds.createVariable(varname,'f8',('time','latitude','longitude',))
    latitudes[:] = lati[:]
    longitudes[:] = loni[:]
    #times[:] = timei[:]
    #variable[:,:,:] = data # time version of the data insert
    
    #variable level attributes
    latitudes.long_name = 'latitude'
    latitudes.standard_name = 'latitude'
    latitudes.units = 'degrees_north'
    latitudes.axis = 'Y'
    longitudes.long_name = 'longitude'
    longitudes.standard_name = 'longitude'
    longitudes.units = 'degrees_east'
    longitudes.axis = 'X'
    
    # Removed time variable - add as required
    #times.long_name = 'time'
    #times.standard_name = 'time'
    #times.units = timeunit
    #times.calendar = 'gregorian'
    #times.axis = 'T'
    
    # Set geospatial global methods [basic method for setting global attr.]
    ncds.geospatial_lat_min = header['latsmin']
    ncds.geospatial_lat_max = header['latsmax']
    ncds.geospatial_lat_units = 'degrees_north'
    ncds.geospatial_lat_resolution = header['resolution']
    ncds.geospatial_lon_min = header['lonsmin']
    ncds.geospatial_lon_max = header['lonsmax']
    ncds.geospatial_lon_units = 'degrees_east'
    ncds.geospatial_lon_resolution = header['resolution']
    
    # set crs
    crs=ncds.createVariable('crs','f4')
    crs.name="GDA94"
    crs.datum= "Geocentric_Datum_of_Australia_1994"
    crs.longitude_of_prime_meridian= 0.0
    crs.inverse_flattening=298.257222101
    crs.semi_major_axis=6378137.0
    crs.semi_minor_axis=6356752.314140356
    crs._CoordinateTransformType="Projection"
    crs._CoordinateAxisTypes="GeoX GeoY"
    
    # Set crs globally [alternative method!]
    setattr(ncds, 'crs:name', "GDA94")
    setattr(ncds, 'crs:datum', "Geocentric_Datum_of_Australia_1994")
    setattr(ncds, 'crs:longitude_of_prime_meridian', 0.0)
    setattr(ncds, 'crs:inverse_flattening', 298.257222101)
    setattr(ncds, 'crs:semi_major_axis', 6378137.0)
    setattr(ncds, 'crs:semi_minor_axis', 6356752.314140356)
    setattr(ncds, 'crs:_CoordinateTransformType', "Projection")
    setattr(ncds, 'crs:_CoordinateAxisTypes', "GeoX GeoY")
    
    
    ncds = updateMetaData(metaData,ncds)

    #write all other meta data to file
    
    
    print(np.amax(data))
    variable = ncds.createVariable(varname,'f4',('latitude','longitude',), fill_value=nodata)
    
    variable[:] = data[:]
    
    print(ncds)
    # Close the file
    ncds.close()
    # Report back
    #print(variable)
    print 'Congratulations, your netCDF file is baked! See:', outfile
    import sys
    #sys.exit()

def updateMetaData(productMetaData,netCDFObj):
        print('Updating Metadata')
        print(productMetaData)

        for columnKey, columnValue in productMetaData.iteritems():
                columnKey=str(columnKey)
                columnKey = columnKey.decode('utf-8','ignore')

                columnValue=str(columnValue)
                columnValue = columnValue.decode('utf-8','ignore')

                writeMetaData=True
                if writeMetaData ==True:
                        setattr(netCDFObj,columnKey,columnValue)
                for name in netCDFObj.ncattrs():
                        print 'Global attr', name, '=', getattr(netCDFObj,name)
    
	return(netCDFObj)



