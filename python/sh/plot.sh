#!/bin/bash
#PBS -l ncpus=16
#PBS -l walltime=00:50:00
#PBS -l mem=16GB
#PBS -P xa5
#PBS -q express

module load hdf5/1.8.10
module load netcdf/4.2.1.1
module load python/2.7.3-matplotlib
module load gdal
module load proj
module use /projects/xa5/modules
module load pythonlib/netCDF4/1.0.4

python /g/data/rr9/sjj576/scripts/plot.py 
