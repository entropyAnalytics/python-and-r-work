#!/bin/bash
#PBS -l ncpus=16
#PBS -l walltime=03:00:00
#PBS -l mem=30GB
#PBS -P xa5
#PBS -q normal

module load hdf5/1.8.10
module load netcdf/4.2.1.1
module load R
module load gdal
module load proj

R --vanilla < ~/test40yearsFix/ewatch.r >~/test40yearsFix/outputewatch

