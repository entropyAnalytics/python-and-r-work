library(raster)
getUpdatedSWC <-function(){
   directoryPath<-'/g/data/rr9/temp40yearsFix2/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m0_aus/mon/land/swco/e_01/1970_2012'
   filePath<-'eMAST_eMAST_R_Package_mon_swco_v1m0_1985_SWC0_v0.nc'
   completePath<-paste0(directoryPath,'/',filePath)
   constantRasterObject<-raster(completePath)
   constantDataFrame<-rasterToPoints(constantRasterObject)
   # add in columns
   elevationVector<-getElevation()
   constantDataFrame<-cbind(constantDataFrame[,1:2],elevationVector,constantDataFrame[,3])
   print(head(constantDataFrame))
}

getElevation<-function(){
  directoryPath<-'/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/fx/land/elev/e_01'
  filePath<-'eMAST_ANUClimate_fx_elev_v1m0.nc'
  completePath<-paste0(directoryPath,'/',filePath)
  elevationRasterObject<-raster(completePath)
  print(elevationRasterObject)
  elevationDataFrame<-rasterToPoints(elevationRasterObject)	
  elevationVector<-elevationDataFrame[,3]
  return (elevationVector)	
}

print(head(getElevation()))
getUpdatedSWC() 
