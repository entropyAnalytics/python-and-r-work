#!/bin/bash
#PBS -l ncpus=16
#PBS -l walltime=00:01:00
#PBS -l mem=1GB
#PBS -P xa5
#PBS -q express

module load hdf5/1.8.10
module load netcdf/4.2.1.1
module load R
module load gdal
module load proj

# create environment variables
echo "Obtain Current Year from env variables"
currentYear=$(printenv currentYearGlobal)
echo "Current year is: " $currentYear
previousYear=$(expr $currentYear - 1)
# set environment variables
echo "Obtain Current job: "
currentJobGlobal=$(printenv currentJobGlobal)
echo "Current Job: "$currentJobGlobal
echo Setting temp environment variables
startYear=1986
echo "Start Year: "$startYear
#export startYear
endYear=1988
echo "End Year: "$endYear
#export endYear
currentJob=""

if [ $currentYear -le $endYear ]
then
	#get current year from global variables
	echo "Current Year: "$currentYear
	echo "Previous Year: " $previousYear
	echo "~/test40yearsFix/outputTest40yearsFix"$currentYear
	# QSUB to go in here. This will load the job into QSUB
	echo "Queuing Job in QSUB"
	# if this is the first job, without dependencies
	if [ $currentYear -eq $startYear ]
	then	
		currentJobGlobal=$(qsub -V ~/test40yearsFix/test40YearsManyJobs2.sh)
		currentJobGlobal=$(echo $currentJobGlobal | sed 's/.r-man2//')
		echo "Initial Current Job: "$currentJobGlobal
		export currentJobGlobal	
	fi
	# if not the first job, then wait for other job to finish
	if [ $currentYear -gt $startYear ] 
	then
		currentJobGlobal=$(qsub -V -W depend=afterok:$currentJobGlobal ~/test40yearsFix/test40YearsManyJobs2.sh)
		currentJobGlobal=$(echo $currentJobGlobal | sed 's/.r-man2//')
		echo "Chained Current Job: "$currentJobGlobal
		export currentJobGlobal
	fi 
	echo "QSUB job  finished"
	echo "Incrementing current year"
	currentYearGlobal=$(expr $currentYear + 1)
	echo "Current Year is now: " $currentYearGlobal
	export currentYearGlobal
fi 




