#!/bin/bash
#PBS -l ncpus=16
#PBS -l walltime=00:02:00
#PBS -l mem=1GB
#PBS -P xa5
#PBS -q express

echo "Script 2 is running"
currentYear=$(printenv currentYearGlobal)
currentJobGlobal=$(printenv currentYearGlobal)


module load hdf5/1.8.10
module load netcdf/4.2.1.1
module load R
module load gdal
module load proj
R --vanilla < ~/test40yearsFix/testGlobals.r >~/test40yearsFix/outputTest40years1$currentYear

echo "Calling script 1"
currentJobGlobal=$(qsub -V -W depend=afterok:$currentJobGlobal ~/test40yearsFix/test40YearsManyJobs.sh |sed 's/.r-man2//')
export currentJobGlobal

