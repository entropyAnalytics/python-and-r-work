library(raster)
library(emast)
test<-FALSE
rowsInTest<-20000
sectors<-20
#sunout<-read.csv('sunOut.csv', header=F)
raster1<-raster('/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/rrad/e_01/1970_2012/eMAST_ANUClimate_mon_rrad_v1m0_197001.nc')
t<-as.data.frame(raster1, xy =TRUE,na.rm=TRUE)
monthlyDataFrame<-cbind(t[ ,c(1,2,3)],t[,3],t[,3],t[,3],t[,3],t[,3],t[,3],t[,3],t[,3],t[,3],t[,3],t[,3])

sectorizeDataFrame<-function(rradDataFrame){
#xmin = min(rradDataFrame[1])
#xmax = max(rradDataFrame[1])
   countOfX <- nrow(rradDataFrame)
   print(paste0('Number of rows: ', countOfX))
   startingRow<-1
   lengthSectorRows<-ceiling(countOfX/sectors)
   print(paste0('Length of Sector Rows: ',lengthSectorRows))
 
   endingRow<-lengthSectorRows
   print(paste0('Ending row: ',endingRow))

   sectorList<-list()
   for (sector in 1:sectors){
     print(sector)
   
   if(sector !=sectors){
      print(paste0('Creating Sector:',sector,' rows: ', startingRow,' to', endingRow))
      sectorList[[sector]]<-rradDataFrame[startingRow:endingRow, ]
       print(paste0('Length of Sector Rows: ',lengthSectorRows))
       print(paste0('Ending row: ',endingRow))
       print(head(sectorList[[sector]]))
       str(sectorList[[sector]])
       print(paste0('Length of Sector Rows: ',lengthSectorRows))
       print(paste0('Ending row: ',endingRow))
      }

   if(sector == sectors){
      print(paste0('Creating Sector:',sector,' rows: ', startingRow,' to', countOfX))
      sectorList[[sector]]<-rradDataFrame[startingRow:countOfX, ]
      print(head(sectorList[[sector]]))
      str(sectorList[[sector]])
      print(paste0('Length of Sector Rows: ',lengthSectorRows))
      print(paste0('Ending row: ',endingRow))
      }

      startingRow<-endingRow + 1
      endingRow<- endingRow+lengthSectorRows

    }
str(sectorList)
   return(sectorList)  
}

convertSectorsToSunHrs<-function(sectorList){
   convertedSectorList<-list()
   index<-1
   for (sector in sectorList){
      print('Converting Sectors')
      convertedSector<-sunshine(sectorList[[index]],output='dataframe')
      convertedSectorList[[index]]<-convertedSector$SunHr
      print(head(convertedSectorList[[index]]))
      str(convertedSectorList[[index]])
      index<-index+1
   }
   str(convertedSectorList)
   return(convertedSectorList)  
}

reassembleSectors<-function(convertedSectorList){
   str(convertedSectorList)
   sunHrsDataFrame<-''
   index<-1
   for (convertedSector in convertedSectorList){
      print(paste0('Adding data frame: ',index))
      if(index==1){
         sunHrsDataFrame<-convertedSectorList[[index]]
         
       }
      if(index>1){
         sunHrsDataFrame<-rbind(sunHrsDataFrame,convertedSector)
      }
   index<-index+1
   }
   return(sunHrsDataFrame)
}


generateMissingData<-function(){
#create raster file from input
rasterPrec<-raster('/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_201008.nc')
print('This is the initial input data frame including missing values')
dataFrameMissing<-as.data.frame(rasterPrec, xy= TRUE, na.rm=FALSE)
colnames(dataFrameMissing)<-c('x','y','z')
print(str(dataFrameMissing))
print(head(dataFrameMissing))
print('This is the missing data from the input data frame')
dataFrameMissing<-subset(dataFrameMissing,is.na(dataFrameMissing[,3]))
print(str(dataFrameMissing))
print(head(dataFrameMissing))
return(dataFrameMissing)
}


# run script
if(test==TRUE){
   monthlyDataFrame<-monthlyDataFrame[1:rowsInTest,]
   }

print(head(monthlyDataFrame))
numberOfColumns<-4
start<-3
print('sun out data frame')

#converts to SUNHrs
print('calling sectorize function')
listRadSectors<-sectorizeDataFrame(monthlyDataFrame)
print('calling conversion function')
listSunHrsSectors<-convertSectorsToSunHrs(listRadSectors)

print('calling rasembling dataframe')
monthlyDataFrame<-reassembleSectors(listSunHrsSectors)
sunoutXY<-monthlyDataFrame[ ,c(1,2)]


#listMonthlyDataFrame<-sunshine(monthlyDataFrame, output='dataframe')
#str(listMonthlyDataFrame)
#sunout<-listMonthlyDataFrame$SunHr
print(head(monthlyDataFrame))
str(monthlyDataFrame)
#sunOut <- sunshine(dataFrameList[[dataFrameName]],output='dataframe')
#print(names(sunOut))
#dataFrameRrad<-sunOut[[1]]
        
# convert data frame into sectors



missing<-generateMissingData()
# create new dataframes and merge with missing
pdf('sunout.pdf')

for(month in start:numberOfColumns ){

print(month)
monthlyDataFrame<-cbind(sunoutXY[c(1,2)], monthlyDataFrame[month])
colnames(monthlyDataFrame)<-c('x','y','z')
str(monthlyDataFrame)
newDataFrame<-rbind(missing, monthlyDataFrame)
str(newDataFrame)
print(paste0('Min: ',min(newDataFrame$x)))
print(paste0('Max: ',max(newDataFrame$x)))
print(paste0('Min: ',min(newDataFrame$y)))
print(paste0('Max: ',max(newDataFrame$y)))


# create raster obect for new data frame

print('Creating Raster file')
   xmin<-112.9
   xmax<-154
   ymin<--43.74
   ymax<--9
   res<-0.01
   columns<-4110
   rows<-3474
   basemap<-raster( ,ncols=columns,nrows=rows,xmin,xmax,ymin,ymax,resolution=0.01)
   coord<-cellFromXY(basemap, cbind(newDataFrame[,1], newDataFrame[,2]))
   basemap[coord]<-newDataFrame[ ,3]
   print("This is the raster object  before the values are added")
   print(basemap)
   plot(basemap)   
   
}
dev.off()

