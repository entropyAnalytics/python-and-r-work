library(emast)
library(Rcpp)
library(sp)
library(raster)
#library(rgdal)
library(ncdf4)

## time variables
starting_year <-1970
ending_year<-1970
year_time_step <-1

#/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_201011.nc'

# missing variable value
missingVariable<--9999

## vars for directories
precipitation_directory<-'/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'
radiation_directory<-'/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'
min_temp_directory<-'/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'
max_temp_directory<-'/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'
precipitationAcronym <-'prec'
radiationAcronym<-'rrad'
min_tempAcronym <-'tmin'
max_tempAcronym<-'tmax'
folderEnding<-'/e_01/1970_2012/'

##vars for file bases
precipitation_file_base<-'eMAST_ANUClimate_mon_prec_v1m0_'
radiation_file_base<-'eMAST_ANUClimate_mon_rrad_v1m0_'
min_temp_file_base<-'eMAST_ANUClimate_mon_tmin_v1m0_'
max_temp_file_base<-'eMAST_ANUClimate_mon_tmax_v1m0_'

#save to list
precList<-c(precipitation_directory,precipitation_file_base,precipitationAcronym,folderEnding)
radList<-c(radiation_directory,radiation_file_base,radiationAcronym,folderEnding)
minList<-c(max_temp_directory,min_temp_file_base,min_tempAcronym,folderEnding)
maxList<-c(max_temp_directory,max_temp_file_base,max_tempAcronym,folderEnding)

#variableList<-list(precList,radList,minList,maxList)

variableList<-list(precList,radList,minList,maxList)
print(variableList)
net_CDF_suffix<-'.nc'

## years / months sequences
years<-seq(starting_year,ending_year)
print(years)
monthsToRun<-2
months<-seq(1,monthsToRun)
print(months)
years_month_directory_base_list<-list()

#global data frames
dataFramePrec <-""
dataFrameRrad <-""
dataFrameTmax <-""
dataFrameTmin <-""
dataFrameAvg <-""
dataFrameList<-list()
tMinRasterObject<-""
tMaxRasterObject<-""
avgRasterObject<-""
tMinSet<-FALSE
tMaxSet<-FALSE

## function that takes directory string builds initial dataframe
makeDataFrames<-function(filePath){
  makeDataFrameStartTime<-proc.time()
  rasterObject<-raster(filePath)
  print(paste('makingRasterFile',filePath,sep=""))
  variableDataFrame<-rasterToPoints(rasterObject)
  colnames(variableDataFrame)<-NULL
  print(paste('converting to dataFrame',filePath,sep=""))
  makeDataFrameEndTime<-proc.time()-makeDataFrameStartTime
  print(paste0('Time to make initial data frame: ',makeDataFrameEndTime))
  return (variableDataFrame)
}

## function that creates data frame but strips out the variable vector only to append to a data frame for months 2-12
appendDataFrames<-function(filePath){
  appendDataFrameStartTime<-proc.time()
  rasterObject<-raster(filePath)
  print(paste('appending raster file',filePath,sep=""))
  variableDataFrame<-rasterToPoints(rasterObject)
  print(paste('converting to dataFrame',filePath,sep=""))
  print('slicing variable column')
  variableColumn<-variableDataFrame[,3]
  print(head(variableColumn))
  appendDataFrameEndTime<-proc.time()-appendDataFrameStartTime
  print(paste0('Time taken to append data frame: ',appendDataFrameEndTime))
  return (variableColumn)
}

makeDataFrameFromRaster<-function(rasterObject){
variableDataFrame<-rasterToPoints(rasterObject)
colnames(variableDataFrame)<-NULL
print('Converting raster file to data frame')
return(variableDataFrame)
}

appendDataFrameFromRaster<-function(rasterFile){
  print('Converting to dataFrame')
  print('slicing variable column')
  variableDataFrame<-rasterToPoints(rasterFile)
  variableColumn<-variableDataFrame[,3]
  print(head(variableColumn))
  return (variableColumn)
}


#function to create average data frame from min and max, converts each to raster then appends data frame
createAverageDataFrame<-function(minDataFrame,maxDataFrame){

averageDataFrameTimeStart<-proc.time()
dataFrame<-minDataFrame[ ,c(1,2)]
columnsInDataFrame<-ncol(minDataFrame)
print(columnsInDataFrame)
columns<-columnsInDataFrame-2
print(columns)
for(column in 1:columns){
   column<-column+2
   print(paste0('Creating min raster :',column))
   rasterMin<-rasterFromXYZ(minDataFrame[,c(1,2,column)])
   print(head(minDataFrame[,c(1,2,column)]))
   print(rasterMin)
   print(paste0('Creating max raster :',column))
   rasterMax<-rasterFromXYZ(maxDataFrame[,c(1,2,column)])
   print(rasterMax)
   print('Calculating Average')
   rasterAverage<-(rasterMin+rasterMax)/2
   print('rasterAverage')
   averageDataFrame<-rasterToPoints(rasterAverage)
   print(head(averageDataFrame))
   dataFrame<-cbind(dataFrame,averageDataFrame[,3])
   }

averateDataFrameTimeTaken<-proc.time()-averagedataFrameTimeStart
print(paste0('Time taken to create average data frame for the year: ', averageDataFrameTimeTaken))
return(dataFrame)
}


## iterate over the year / month / directory / base to produce file name
#year time
yearTimeStart<-proc.time()
print('Starting load:')    

for(year in years){ 
    for (list in variableList){
        for(month in months){ 
          if(month<10){
              fileName<-paste(list[1],list[3],list[4],list[2],year,'0',month,net_CDF_suffix,sep="")
          }
        
          if(month>=10){
             fileName<-paste(list[1],list[3],list[4],list[2],year,month,net_CDF_suffix,sep="")
          }
       print(fileName) 
       
       #create dataFrame based on directory
        if(month ==1){
           dataFrame1<-makeDataFrames(fileName)
           print(head(dataFrame1))
        }
           
         if(month >1){
            dataFrame1<-cbind(dataFrame1,appendDataFrames(fileName))
            print(head(dataFrame1))
         }     
         print(list[3])   
          
         if(identical(list[3],'prec')){
            dataFramePrec<-dataFrame1
         }  

         if(identical(list[3],'rrad')){
            dataFrameRrad<-dataFrame1
         } 
         
         if(identical(list[3],'tmin')){
            dataFrameTmin<-dataFrame1
         }

         if(identical(list[3],'tmax')){
            dataFrameTmax<-dataFrame1 
          
           if(month==monthsToRun){
             #create constant dataframe
             dataFrameConstants<-dataFrameTmin[,c(1,2)]
             print('Creating constant data frame')
	     dataFrameConstants<-cbind(dataFrameConstants,0,150,0)
             print(head(dataFrameConstants))
                     
             # create average dataframe
             dataFrameAvg<-createAverageDataFrame(dataFrameTmin,dataFrameTmax)  


             #remove missing dataFrame values
             print('removing missing values for precp')
	     print(nrow(dataFramePrec))
             dataFramePrec<-subset(dataFramePrec,dataFramePrec[ ,3]!=missingVariable)
	     print(nrow(dataFramePrec)) 
             
             print('removing missing values for rrad')
	     print(nrow(dataFrameRrad))
             dataFrameRrad<-subset(dataFrameRrad,dataFrameRrad[ ,3]!=missingVariable)
	     print(nrow(dataFrameRrad)) 
             
             print('removing missing values for avg temp')
	     print(nrow(dataFrameAvg))
             dataFrameAvg<-subset(dataFrameAvg,dataFrameAvg[ ,3]!=missingVariable)
	     print(paste0('Average temperature rows: ',nrow(dataFrameAvg)))
 
 	     # save to list
	   
	     precipListIndexName<-paste0('prec_',year)
	     rradListIndexName<-paste0('rad_',year)
	     avgTempIndexName<-paste0('avgTemp_',year)
	     constantIndexName<-paste0('constant_',year)
	     dataFrameList[[precipListIndexName]]<-dataFramePrec
	     dataFrameList[[rradListIndexName]]<-dataFrameRrad
             dataFrameList[[avgTempIndexName]]<-dataFrameAvg
             dataFrameList[[constantIndexName]]<-dataFrameConstants
	     print(str(dataFrameList))
             } # final month if
         }# end if for tmax, the last variable
      }# end month for
   } # end variable for
 } # end year loop 

timeTaken<-proc.time()-yearTimeStart
print(paste('Time taken:',timeTaken))
