library(raster)
rasterObjectChil<-raster('/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_201008.nc')

print(rasterObjectChil)

rasterChilDataFrame<-as.data.frame(rasterObjectChil, xy=TRUE)
str(rasterChilDataFrame)
print(head(rasterChilDataFrame))
print(min(rasterChilDataFrame$Monthly.total.precipitation, na.rm=TRUE))
print(max(rasterChilDataFrame$Monthly.total.precipitation, na.rm=TRUE))


missing<-rasterChilDataFrame[rasterChilDataFrame$Monthly.total.precipitation==-9999, ]
str(missing)
print(head(missing)) 
