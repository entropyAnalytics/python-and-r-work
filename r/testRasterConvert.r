library(raster)
monthlyDataFrame<-read.csv('/home/576/sjj576/AET_1970.csv')
print(head(monthlyDataFrame))
monthlyDataFrame<-monthlyDataFrame[,c(1,2,3)]
#testRaster <-raster('/g/data/rr9/temp40years/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/chiltest/eMAST_ANUClimate_mon_chil_v1m0_201008_v0.nc')
# previous way of making raster file, did not account fo rmissing data
      
      columns<-length(unique(monthlyDataFrame[,2]))
      
      rows<-length(unique(monthlyDataFrame[ ,1]))
      
      print(columns)
      print(class(columns))
      print(rows)
      print(class(rows))
      xmin<-as.numeric(min(monthlyDataFrame[,1]))
      print(paste0('xmin: ',xmin))
      print(class(xmin))
      xmax<-as.numeric(max(monthlyDataFrame[,1]))
      print(paste0('xmax: ',xmax))
      print(class(xmax))
      ymin<-as.numeric(min(monthlyDataFrame[,2]))
      print(paste0('ymin: ',ymin))
      print(class(ymin))
      ymax <-as.numeric(max(monthlyDataFrame[,2])) 
      print(paste0('ymax: ',ymax))
      print(class(ymax))
      print('creating base layer')
      basemap<-raster( ,columns,rows,xmin,xmax,ymin)
            print("This is the raster object  before the values are added")
      print(basemap)
      coord<-cellFromXY(basemap, cbind(monthlyDataFrame[,1], monthlyDataFrame[,2])) 
      basemap[coord]<-monthlyDataFrame[ ,3]
      res(basemap)<-0.01
 
      print("This is the raster object after the values are added") 
      print(basemap)
      pdf('testRaster.pdf') 
       plot(basemap)
      dev.off() 

#testRaster<-rasterFrom[,c(1,2,3)])
#print(testRaster)

