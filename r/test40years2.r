library(emast)
library(Rcpp)
library(sp)
library(raster)
#library(rgdal)
library(ncdf4)

## time variables
starting_year <-1970
ending_year<-2012
year_time_step <-1

#/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_201011.nc'

# missing variable value
missingVariable<--9999

## vars for directories
precipitation_directory<-'/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'
radiation_directory<-'/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'
min_temp_directory<-'/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'
max_temp_directory<-'/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'
precipitationAcronym <-'prec'
radiationAcronym<-'rrad'
min_tempAcronym <-'tmin'
max_tempAcronym<-'tmax'
folderEnding<-'/e_01/1970_2012/'

##vars for file bases
precipitation_file_base<-'eMAST_ANUClimate_mon_prec_v1m0_'
radiation_file_base<-'eMAST_ANUClimate_mon_rrad_v1m0_'
min_temp_file_base<-'eMAST_ANUClimate_mon_tmin_v1m0_'
max_temp_file_base<-'eMAST_ANUClimate_mon_tmax_v1m0_'

#save to list
precList<-c(precipitation_directory,precipitation_file_base,precipitationAcronym,folderEnding)
radList<-c(radiation_directory,radiation_file_base,radiationAcronym,folderEnding)
minList<-c(max_temp_directory,min_temp_file_base,min_tempAcronym,folderEnding)
maxList<-c(max_temp_directory,max_temp_file_base,max_tempAcronym,folderEnding)

#variableList<-list(precList,radList,minList,maxList)

variableList<-list(precList,radList,minList,maxList)
print(variableList)
net_CDF_suffix<-'.nc'

## years / months sequences
years<-seq(starting_year,ending_year)
print(years)
monthsToRun<-12
months<-seq(1,monthsToRun)
print(months)
years_month_directory_base_list<-list()

#global data frames
dataFramePrec <-""
dataFrameRrad <-""
dataFrameTmax <-""
dataFrameTmin <-""
dataFrameAvg <-""
dataFrameList<-list()
tMinRasterObject<-""
tMaxRasterObject<-""
avgRasterObject<-""
tMinSet<-FALSE
tMaxSet<-FALSE

## function that takes directory string builds initial dataframe
makeDataFrames<-function(filePath){
  startTime<-proc.time()
  rasterObject<-raster(filePath)
  print(paste('makingRasterFile',filePath,sep=""))
  variableDataFrame<-rasterToPoints(rasterObject)
  colnames(variableDataFrame)<-NULL
  print(paste('converting to dataFrame',filePath,sep=""))
  totalTime<-proc.time()-startTime
  print(paste0('Time taken to create data frame: ',totalTime))
  return (variableDataFrame)
}

## function that creates data frame but strips out the variable vector only to append to a data frame for months 2-12
appendDataFrames<-function(filePath){
  startTime<-proc.time()
  rasterObject<-raster(filePath)
  print(paste('appending raster file',filePath,sep=""))
  variableDataFrame<-rasterToPoints(rasterObject)
  print(paste('converting to dataFrame',filePath,sep=""))
  print('slicing variable column')
  variableColumn<-variableDataFrame[,3]
  print(head(variableColumn))
  totalTime<-proc.time()-startTime
  print(paste0('Time taken to append data frame',totalTime))
  return (variableColumn)
}

makeDataFrameFromRaster<-function(rasterObject){
variableDataFrame<-rasterToPoints(rasterObject)
colnames(variableDataFrame)<-NULL
print('Converting raster file to data frame')
return(variableDataFrame)
}

appendDataFrameFromRaster<-function(rasterFile){
  print('Converting to dataFrame')
  print('slicing variable column')
  variableDataFrame<-rasterToPoints(rasterFile)
  variableColumn<-variableDataFrame[,3]
  print(head(variableColumn))
  return (variableColumn)
}


#function to create average data frame from min and max, converts each to raster then appends data frame
createAverageDataFrame<-function(minDataFrame,maxDataFrame){
startTime<-proc.time()
dataFrame<-minDataFrame[ ,c(1,2)]
columnsInDataFrame<-ncol(minDataFrame)
print(columnsInDataFrame)
columns<-columnsInDataFrame-2
print(columns)
for(column in 1:columns){
   column<-column+2
   print(paste0('Creating min raster :',column))
   rasterMin<-rasterFromXYZ(minDataFrame[,c(1,2,column)])
   print(head(minDataFrame[,c(1,2,column)]))
   print(rasterMin)
   print(paste0('Creating max raster :',column))
   rasterMax<-rasterFromXYZ(maxDataFrame[,c(1,2,column)])
   print(rasterMax)
   print('Calculating Average')
   rasterAverage<-(rasterMin+rasterMax)/2
   print('rasterAverage')
   averageDataFrame<-rasterToPoints(rasterAverage)
   print(head(averageDataFrame))
   dataFrame<-cbind(dataFrame,averageDataFrame[,3])
   }
totalTime<-proc.time()-startTime
print(paste0('Total Time to create average data frame: ',totalTime))
return(dataFrame)
}


## iterate over the year / month / directory / base to produce file name
#year time
yearTimeStart<-proc.time()
print('Starting load:')    

for(year in years){ 
    for (list in variableList){
        for(month in months){ 
          if(month<10){
              fileName<-paste(list[1],list[3],list[4],list[2],year,'0',month,net_CDF_suffix,sep="")
          }
        
          if(month>=10){
             fileName<-paste(list[1],list[3],list[4],list[2],year,month,net_CDF_suffix,sep="")
          }
       print(fileName) 
       
       #create dataFrame based on directory
        if(month ==1){
           dataFrame1<-makeDataFrames(fileName)
           print(head(dataFrame1))
        }
           
         if(month >1){
            dataFrame1<-cbind(dataFrame1,appendDataFrames(fileName))
            print(head(dataFrame1))
         }     
         print(list[3])   
          
         if(identical(list[3],'prec')){
            dataFramePrec<-dataFrame1
         }  

         if(identical(list[3],'rrad')){
            dataFrameRrad<-dataFrame1
         } 
         
         if(identical(list[3],'tmin')){
            dataFrameTmin<-dataFrame1
         }

         if(identical(list[3],'tmax')){
            dataFrameTmax<-dataFrame1 
          
           if(month==monthsToRun){
             #create constant dataframe
             dataFrameConstants<-dataFrameTmin[,c(1,2)]
             print('Creating constant data frame')
	     dataFrameConstants<-cbind(dataFrameConstants,0,150,0)
             print(head(dataFrameConstants))
                     
             # create average dataframe
             dataFrameAvg<-createAverageDataFrame(dataFrameTmin,dataFrameTmax)  


             #remove missing dataFrame values
             print('removing missing values for precp')
	     print(nrow(dataFramePrec))
             dataFramePrec<-subset(dataFramePrec,dataFramePrec[ ,3]!=missingVariable)
	     print(nrow(dataFramePrec)) 
             
             print('removing missing values for rrad')
	     print(nrow(dataFrameRrad))
             dataFrameRrad<-subset(dataFrameRrad,dataFrameRrad[ ,3]!=missingVariable)
	     print(nrow(dataFrameRrad)) 
             
             print('removing missing values for avg temp')
	     print(nrow(dataFrameAvg))
             dataFrameAvg<-subset(dataFrameAvg,dataFrameAvg[ ,3]!=missingVariable)
	     print(paste0('Average temperature rows: ',nrow(dataFrameAvg)))
 
 	     # save to list
	   
	     precipListIndexName<-paste0('prec_',year)
	     rradListIndexName<-paste0('rad_',year)
	     avgTempIndexName<-paste0('avgTemp_',year)
	     constantIndexName<-paste0('constant_',year)
	     dataFrameList[[precipListIndexName]]<-dataFramePrec
	     dataFrameList[[rradListIndexName]]<-dataFrameRrad
             dataFrameList[[avgTempIndexName]]<-dataFrameAvg
             dataFrameList[[constantIndexName]]<-dataFrameConstants
	     print(str(dataFrameList))
             } # final month if
         }# end if for tmax, the last variable
      }# end month for
   } # end variable for
 } # end year loop 

timeTaken<-proc.time()-yearTimeStart
print(paste('Time taken:',timeTaken))



# start of run model

# test mode
test<-FALSE
writeToFile<-TRUE
createRaster<-TRUE
createDirectories<-TRUE
irregularDataFrame<-FALSE
rowsToRemove<-33
if (test==TRUE){

   ## create test list
   long<-rep(1:20, each=20)
   lat<-rep(1:20,20)
   variable<-seq(1:400)
   baseLatLong<-data.frame(long,lat)
   dataFramePrec<-cbind(baseLatLong,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable)
   dataFrameRad<-cbind(baseLatLong,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable)
   dataFrameAvgTemp<-cbind(baseLatLong,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable)
   dataFrameConst<-data.frame(lat,long,0,150,0)
   dataFrameList<-list()

   #remove column names
   colnames(dataFramePrec)<-NULL
   colnames(dataFrameRad)<-NULL
   colnames(dataFrameAvgTemp)<-NULL
   colnames(dataFrameConst)<-NULL

   #add to list
   yearStart<-1970
   yearEnd<-1970
   range<-range(yearStart:yearEnd)

   for(year in yearStart:yearEnd){
      #names for test data list
      namePrec<-paste0('prec_',year)
      nameRad<-paste0('rad_',year)
      nameAvg<-paste0('avgTemp_',year)
      nameConstant<-paste0('constant_',year)
      
      # add test data frames to list iteratively 
      dataFrameList[[namePrec]]<-dataFramePrec
      dataFrameList[[nameRad]]<-dataFrameRad
      dataFrameList[[nameAvg]]<-dataFrameAvgTemp
      dataFrameList[[nameConstant]]<-dataFrameConst
      }      
      str(dataFrameList)
}# end test data generation

#global variables for dataFrames
dataFramePrec<-""
dataFrameRrad<-""
dataFrameAvg<-""
dataFrameConst<-""

#global list to store dataframes
dataFrameNames<-names(dataFrameList)

#when variable counter gets to four, the model will be run
variableCounter<-0

#function to get monthly data frames from yealy dataFrames, create raster files, create directory and file name, write to file
getMonthlyDataFrames<-function(yearlyDataFrame,variableName,year){
   startTime<-proc.time()
   numberOfColumns<-ncol(yearlyDataFrame)
   columns <-seq(3:numberOfColumns)
   dataFrameBase<-yearlyDataFrame[ ,c(1,2)]
  
   #iterate monthly columns, make new dataFrames, create raster file, create directory and file name, write to file 
   for(month in columns){
       monthlyDataFrame<-cbind(dataFrameBase,yearlyDataFrame[,month])
       print(month)
       print(head(monthlyDataFrame))
  
       #from monthly dataFrame save to raster object
       monthlyRasterObject<-convertToRaster(monthlyDataFrame)
 
       ##for the monthly data frame, get directory and filename 
       directoryAndFile<-generateDirectoryAndFileName(variableName,year,month)
       directoryName<-directoryAndFile$directory
       fileName<-directoryAndFile$file
       
       ## for the monthly raster object save to file
       saveToFile(monthlyRasterObject,directoryName,fileName)
       totalTime<-proc.time()-startTime
     
   } 
}

#function to get non-monthly dataFrames, create raster files, create directory and filename, write to file
getNonMonthlyDataFrames<-function(multiVariableDataFrame,variableName,year){   
   numberOfColumns<-ncol(multiVariableDataFrame)
   numberOfColumns<-numberOfColumns-2
   print(paste0('Number of Columns: ',numberOfColumns))
   columns<-seq(1:numberOfColumns)
   dataFrameBase<-multiVariableDataFrame[, c(1,2)]
  
 ##iterate through columns, get the name of each colum, make a new data frame, create raster file, create directory and file name, write to file
   for(column in columns){
      column<-column+2
      print(paste0('Current Column: ',column))
      variableDataFrame<-cbind(dataFrameBase,multiVariableDataFrame[ , column])
      columnNameToSave<-colnames(multiVariableDataFrame)[column]
      columnNameToSave<-paste0('_',columnNameToSave)
      print(columnNameToSave)
      print(head(variableDataFrame))
 
      # save to raster object
      if(createRaster==TRUE){
      variableRasterObject<-convertToRaster(variableDataFrame)
      }
    
      if(createRaster==FALSE){
      print('Set createRaster config to TRUE to create raster')
      }
      
      ##for the variable data frame, get directory and filename 
      directoryAndFile<-generateDirectoryAndFileName(variableName,year,columnNameToSave)
      directoryName<-directoryAndFile$directory
      fileName<-directoryAndFile$file
      ## for the monthly raster object save to file
      saveToFile(variableRasterObject,directoryName,fileName)
   }
}


#function to generate name directory path and name of file
# "temp/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197001.nc"
generateDirectoryAndFileName<-function(variableAcronym,year,month){
   print('generating directory and filename')
   baseDirectory<-'g/data/rr9/temp40years/'
   directoryStructure1<-'Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'
   short<-paste0(variableAcronym,'/')
   directoryStructure2<-'e_01/1970_2012/'
   fileBase1<-'eMAST_ANUClimate_mon_'
   fileBase2<-'_v1m0_'
   fileSuffix<-'_v0.nc'
   
   ##append 0 to month if 1-9
   if(month<10){
      month<-paste0(0,month)
   }
   
   # create directory and file name
   directoryName<-paste0('/',baseDirectory,directoryStructure1,short,directoryStructure2)
   fileName<-paste0(fileBase1,variableAcronym,fileBase2,year,month,fileSuffix)
   
   namesList<-list(directory=directoryName,file=fileName)
   print(namesList$directory)
   print(namesList$file)
   return(namesList)
}

#function to convert to raster object
convertToRaster<-function(monthlyDataFrame){
   startTime<-proc.time()
   print('Converting to monthly dataframe to monthly raster')
   
   if(createRaster==FALSE){
   print('To enable creating raster object, change config createRaster to TRUE')
   } 
   if(createRaster==TRUE){
     # new way of creating raster file with rasterFromXYZ
     
       if(irregularDataFrame==TRUE){
          print('removing rows to make irregular')
          rows<-nrow(monthlyDataFrame)
          print(rows)
          monthlyDataFrame<-monthlyDataFrame[-sample(rows,rowsToRemove), ]
          print(paste0('New Rows: ',nrow(monthlyDataFrame)))
        }
     
     #previous way of making raster file, could not account for missing data
     #rasterObject<-rasterFromXYZ(monthlyDataFrame)
     #return(rasterObject)
   
     columns<-length(unique(monthlyDataFrame[,2]))
     rows<-length(unique(monthlyDataFrame[ ,1]))
     print(columns)
     print(class(columns))
     print(rows)
     print(class(rows))
     xmin<-as.numeric(min(monthlyDataFrame[,1]))
     print(paste0('xmin: ',xmin))
     print(class(xmin))
     xmax<-as.numeric(max(monthlyDataFrame[,1]))
     print(paste0('xmax: ',xmax))
     print(class(xmax))
     ymin<-as.numeric(min(monthlyDataFrame[,2]))
     print(paste0('ymin: ',ymin))
     print(class(ymin))
     ymax <-as.numeric(max(monthlyDataFrame[,2])) 
     print(paste0('ymax: ',ymax))
     print(class(ymax))
     print('creating base layer')
     basemap<-raster( ,columns,rows,xmin,xmax,ymin)
     print("This is the raster object  before the values are added")
     print(basemap)
     coord<-cellFromXY(basemap, cbind(monthlyDataFrame[,1], monthlyDataFrame[,2])) 
     basemap[coord]<-monthlyDataFrame[ ,3]
     print("This is the raster object after the values are added") 
     print(basemap)
     totalTime<-proc.time()-startTime
     print(paste0('Total time to create raster file',totalTime))
     return(basemap)
     }
}  


#function to save to save to file
saveToFile<-function(rasterFile,directory,file){
    completeFilePath<-paste0(directory,file)
    
    #check to see if directory exists, if it does not, then create it
    # dir.create() will show a warning if directory alreday exists but not an error
    print(paste0('Creating directory: ',directory))
    if(createDirectories==FALSE){
      print('set createDirectories to TRUE to create directories')
    }
    
    if(createDirectories==TRUE){
       dir.create(directory,recursive=TRUE)   
     }
    
    #check to see if file exists, if it does not, write write file
    if(file.exists(completeFilePath)){
       print(paste(file,' exists but is being removed'))
       file.remove(completeFilePath)
    }
    
    if(writeToFile==FALSE){
    print(paste0('Raster file will be written to: ',directory,file,' when write config enabled'))
    }

    if(writeToFile==TRUE){
    print(paste0('Writing raster file to: ',directory,file))
    writeRaster(rasterFile,completeFilePath)
    }
}

for (dataFrameName in dataFrameNames){
   variableCounter<-variableCounter+1
   yearAndName<-strsplit(dataFrameName,"_")
   variableName<-yearAndName[[1]][1]
   year<-yearAndName[[1]][2]
	
   if(variableName=='prec'){
   dataFramePrec<-dataFrameList[[dataFrameName]]
   print(paste(variableCounter,variableName))
   print(head(dataFramePrec))
   }
   
   if(variableName=='rad'){
   dataFrameRrad<-dataFrameList[[dataFrameName]]
   sunOut <- sunshine(dataFrameList[[dataFrameName]],output='dataframe')
   print(names(sunOut))
   dataFrameRrad<-sunOut[[1]]
   colnames(dataFrameRrad)<-NULL
   print(head(dataFrameRrad))
   print(paste(variableCounter,variableName))
   }

   if(variableName=='avgTemp'){
   dataFrameAvg<-dataFrameList[[dataFrameName]]
   print(paste(variableCounter,variableName))
   print(head(dataFrameAvg))
   }

   if(variableName=='constant'){
   dataFrameConst<-dataFrameList[[dataFrameName]]
   print(paste(variableCounter,variableName))
   print(head(dataFrameConst))
   }
   
   if (variableCounter==4){
   print("Will run model here after dataFrames are obtained for each year")
   startTime<-proc.time()
   output<- grid.stash(dataFrameAvg,dataFramePrec,dataFrameRrad,dataFrameConst,output='dataframe')
   totalTime<-proc.time()-startTime
   print(paste0('Time taken to run model: ',totalTime))
   #reset variable counter
   variableCounter=0
   print(str(output))
   
   
   # need to save to variable names as follows:   
   #Total, AET, EET, PET, DET, PAR, MI, RO, Alpha, GDD0, GDD5, GDD10, Chill, SWC0
   newVariableNames<-names(output)
   
   #for each new variable produced by the model, split into monthly data frames, generate directory and filename, convert to raster, save to file
   for (newVariableName in newVariableNames){

     #total is not a requierd data product
     #if(newVariableName =='Total'){
     #   print('Saving Total')
     #   getNonMonthlyDataFrames(output[[newVariableName]],'totl',year)
     #}

     if(newVariableName =='AET'){
        print('Saving AET')
        getMonthlyDataFrames(output[[newVariableName]],'etac',year)
     }

     if(newVariableName =='EET'){
        print('Saving EET')
        getMonthlyDataFrames(output[[newVariableName]],'eteq',year) 
     }

     if(newVariableName =='PET'){     
        print('Saving PET')
        getMonthlyDataFrames(output[[newVariableName]],'etpo',year)
     }

    # if(newVariableName =='DET'){     
    #    print('Saving DET')
    #    getMonthlyDataFrames(output[[newVariableName]],'etde',year) 
    # }

     if(newVariableName =='PAR'){     
        print('Saving PAR')
        getMonthlyDataFrames(output[[newVariableName]],'parr',year) 
     }
     
     if(newVariableName =='MI'){     
        print('Saving MI')
        getMonthlyDataFrames(output[[newVariableName]],'moin',year) 
     }

     if(newVariableName =='RO'){     
        print('Saving RO')
        getMonthlyDataFrames(output[[newVariableName]],'runo',year) 
     }
     
     if(newVariableName =='ALPHA'){     
        print('Saving ALPHA')
        getMonthlyDataFrames(output[[newVariableName]],'alph',year) 
     }

     if(newVariableName =='GDD0'){     
        print('Saving GDD0')
        getMonthlyDataFrames(output[[newVariableName]],'gd00',year) 
     }

     if(newVariableName =='GDD5'){     
        print('Saving GDD5')
        getMonthlyDataFrames(output[[newVariableName]],'gd05',year) 
     }

     if(newVariableName =='GDD10'){     
        print('Saving GDD10')
        getMonthlyDataFrames(output[[newVariableName]],'gd10',year)
     }

     if(newVariableName =='Chill'){     
        print('Saving Chill')
        getNonMonthlyDataFrames(output[[newVariableName]],'chil',year)
 
     
     }

     if(newVariableName=='SWC0'){     
        print('Saving SWC0')
        getNonMonthlyDataFrames(output[[newVariableName]],newVariableName,year) 
     }
    
   }
}

}
