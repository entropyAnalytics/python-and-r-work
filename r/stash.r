brary(raster, quietly=T)
library(stringr, quietly=T)
library(emast, quietly=T)
library(DBI, quietly=T)
library(tcltk, quietly=T)

# author: Rhys Whitley
# date: Mar 2015
# institution: emast

buildSaveDir <- function(variable) {
# if the folder doesn't exist create it; model outputs will be saved here
    dirPath <- paste("../model_outputs/", variable, sep="")
    dir.create( dirPath, recursive=T, showWarnings = FALSE )
    return(dirPath)
}

multiStash <- function( path.list, swc0=-9999. ) {
# wrapper to handle multiple years being pass to the stash model

    # flattens from list to string
    path.list2 <- unlist(path.list)
    # get year from file path
    year <- str_extract(path.list2[1], perl("^\\d+"))
    # echo user
    cat("Running STASH for year: ", year)

    # read in data using the SQL wrapper function (very fast)
    qSQL <- "SELECT * FROM file"
    tmin <- sqldf::read.csv.sql( file=path.list2[1], sql=qSQL, header=T )
    tmax <- sqldf::read.csv.sql( file=path.list2[2], sql=qSQL, header=T )
    rain <- sqldf::read.csv.sql( file=path.list2[3], sql=qSQL, header=T )
    fsun <- sqldf::read.csv.sql( file=path.list2[4], sql=qSQL, header=T )

    # mean annual temperature
    tmean <- cbind( tmin[,1:2], 
                     (tmin[3:ncol(tmin)]+tmin[3:ncol(tmin)])/2 )

    # setup land surface characteristics file
    gchar <- cbind( tmin[,1:2], elev=0, fc=140, swc0=swc0 )

    # run the STASH model
    time <- system.time( resStash <- grid.stash( tmean, rain, fsun, gchar, output="dataframe" ) )
    # monitor and echo run time back to the user
    cat("\truntime = ", time[1], " sec\n")

    # only grab those matrices we want to write to disk (first and last elements of list can be ignored)
    takeThis <- 2:(length(resStash)-1)
    # grab the variable names for the wanted outputs
    folNames <- names(resStash)[takeThis]
    # column headers for each matrix to be written to csv
    matHead <- c("lon","lat",paste("m",1:12,sep=""))

    # cycle through each matrix and write it to a csv file
    for( i in takeThis ) {
        # create an outputs directory if it doesn't exist
        dpath <- buildSaveDir( folNames[i-1] )
        # build a string denoting the save path and file name
        fname <- paste(folNames[i-1],"_",year,"",".csv",sep="")
        fullPath <- paste(dpath,"/", fname, sep="")
        # write to file
        write.table( resStash[[i]], file=fullPath, sep=",", col.names=matHead, row.names=F )
    }

    # temporarily save the initial soil water conditions for the following year
    swci0 <- resStash$SWC0[,3]

    # dump all inputs and outputs (now that they've been written to file)
    rm(tmin)
    rm(tmax)
    rm(rain)
    rm(fsun)
    rm(resStash)

    # return end of year SWC to pass onto next year as initial conditions
    return( swci0 )
}

# set the working directory for where all the inputs are stored
setwd("~/Work/Research_Work/Climatologies/ANUCLIM/5km/ASCII/model_inputs/")

# grab all the meteorologies (inputs) and group them into vectors for each variable
tm0.path <- gsub("^./", "", list.files(pattern="Tmin*", recursive=T, full.names=T))
tmx.path <- gsub("^./", "", list.files(pattern="Tmax*", recursive=T, full.names=T))
ppt.path <- gsub("^./", "", list.files(pattern="Precipitation*", recursive=T, full.names=T))
sun.path <- gsub("^./", "", list.files(pattern="SunHr*", recursive=T, full.names=T))

# combine them into one matrix that can be passed to the multiStash wrapper function, where
# each row denotes a year
all.path <- cbind(tm0.path, tmx.path, ppt.path, sun.path)

# number of years to run starting from 1970
testYears <- nrow(all.path)

# create an empty vector to accept the returning soil water conditions to pass onto  the next year
gcRun <- vector("list", testYears)

# Run the model for all years, spinning up on the first year - the soil water conditions of which are
# passed on to subsequent years
for( i in 1:testYears ) {
    # if it is the first year (1970) then do the spin-up on the model
    if( i==1 ) {
        swcI0 <- -9999.
    # otherwise pass last years soil water conditions as the initial conditions to this year's run
    } else {
        swcI0 <- gcRun[[i-1]]
    }
    # run the model
    gcRun[[i]] <- multiStash( all.path[i,], swc0=swcI0 )
}



