library(raster)
output<-read.csv("model.Output.csv")
colnames(output)<-c('x','y','z')
missing<-read.csv('missing.csv')

output<-output[,c(2,3,4)]
colnames(missing)<-c('x','y','z')

missing<-missing[,c(2,3,4)]
print(head(missing))
str(missing)
print(unique(missing$x))
print(unique(missing$y))

print(head(output))
str(missing)
print(unique(output$x))
print(unique(output$y))


new<-rbind(missing,output)

print(head(new))
str(new)
print(unique(new$x))
print(unique(new$y))

xmin<-112.9
xmax<-154
ymin<--43.74
ymax<--9
res<-0.01
columns<-(xmax-xmin)/res
rows<-(ymax-ymin)/res
basemap<-raster( ,columns,rows,xmin,xmax,ymin,ymax,resolution=0.01)
coord<-cellFromXY(basemap, cbind(new[,1], new[,2]))
basemap[coord]<-new[ ,3]
print(basemap)
res(basemap)
extent(basemap)
pdf('testMerge.pdf')
plot(basemap)
dev.off()

