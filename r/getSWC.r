library(raster)
getUpdatedSWC <-function(){
   directoryPath<-'/g/data/rr9/temp40yearsFix2/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m0_aus/mon/land/swco/e_01/1970_2012'
   filePath<-'eMAST_eMAST_R_Package_mon_swco_v1m0_1985_SWC0_v0.nc'
   completePath<-paste0(directoryPath,'/',filePath)
   constantRasterObject<-raster(completePath)
   constantDataFrame<-rasterToPoints(constantRasterObject)
   # add in columns
   constantDataFrame<-cbind(constantDataFrame,0,150)
   constantDataFrame<-cbind(constantDataFrame[,1:2],constantDataFrame[,4:5],constantDataFrame[,3])
   print(head(constantDataFrame))
}

getUpdatedSWC()
