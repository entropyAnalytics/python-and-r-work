library(raster)
#monthlyDataFrame<-read.csv('/home/576/sjj576/AET_1970.csv')
monthlyRasterObjectChil<-raster('/g/data/rr9/temp40years/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/chil/e_01/1970_2012/eMAST_ANUClimate_mon_chil_v1m0_201008_v0.nc')


print('This is the output from the model')
print(monthlyRasterObjectChil)
print('The resolution is :')
print(res(monthlyRasterObjectChil))
print('The extent is:')
print(extent(monthlyRasterObjectChil))

xmin(monthlyRasterObjectChil)<-112.9
xmax(monthlyRasterObjectChil)<-154
ymin(monthlyRasterObjectChil)<--43.74 
ymax(monthlyRasterObjectChil) <--9

print('This is the updated output from the model')
print(monthlyRasterObjectChil)
print('The resolution is :')
print(res(monthlyRasterObjectChil))
print('The extent is:')
print(extent(monthlyRasterObjectChil))


monthlyDataFrame<-as.data.frame(monthlyRasterObjectChil,xy=TRUE,na.rm=FALSE)
print(head(monthlyDataFrame))

monthlyDataFrame<-monthlyDataFrame[,c(1,2,3)]
colnames(monthlyDataFrame)<-c('x','y','z')
print('Output of model dataframe:')
print(str(monthlyDataFrame))
print(head(monthlyDataFrame))

## obtain file netcdfFile that has  missing data
## open raster file
print('The inputs of the model were the following:')
print('PREC')
rasterPrec<-raster('/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_201008.nc')
## convert to data frame

print('Prec')
print('The resolution is :')
print(res(rasterPrec))
print('The extent is:')
print(extent(rasterPrec))
print(paste0('Columns: ',ncol(rasterPrec)))
print(paste0('Rows: ',nrow(rasterPrec)))

rasterRrad<-raster('/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/rrad/e_01/1970_2012/eMAST_ANUClimate_mon_rrad_v1m0_201008.nc')

print('RRAD')
print('The resolution is :')
print(res(rasterRrad))
print('The extent is:')
print(extent(rasterRrad))


rasterTmin<-raster('/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/tmin/e_01/1970_2012/eMAST_ANUClimate_mon_tmin_v1m0_201008.nc')

print('TMIN')

print('The resolution is :')
print(res(rasterTmin))
print('The extent is:')
print(extent(rasterTmin))

rasterTmax<-raster('/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/tmax/e_01/1970_2012/eMAST_ANUClimate_mon_tmax_v1m0_201008.nc')

print('TMAX')

print('The resolution is :')
print(res(rasterTmax))
print('The extent is:')
print(extent(rasterTmax))

print('This is the initial input data frame including missing values')
dataFrameMissing<-as.data.frame(rasterPrec, xy= TRUE, na.rm=FALSE)
colnames(dataFrameMissing)<-c('x','y','z')
print(str(dataFrameMissing))
print(head(dataFrameMissing))
print('This is the missing data from the input data frame')
dataFrameMissing<-subset(dataFrameMissing,is.na(dataFrameMissing[,3]))
print(str(dataFrameMissing))
print(head(dataFrameMissing))

print(paste('Min X',min(dataFrameMissing$x)))
print(paste('Max X',max(dataFrameMissing$x)))
print(paste('Min Y',min(dataFrameMissing$y)))
print(paste('Max Y',max(dataFrameMissing$y)))


print('Dataframe to be merged with')
print(str(monthlyDataFrame))
print(head(monthlyDataFrame))
monthlyDataFrame<-rbind(monthlyDataFrame,dataFrameMissing)
print('The combined data frame is the following')
print(str(monthlyDataFrame))
print(head(monthlyDataFrame))


      
columns<-length(unique(monthlyDataFrame[,2]))
xmin<-112.9
xmax<-154
ymin<--43.74
ymax<--9
res<-0.01
columns<-(xmax-xmin)/res
rows<-(ymax-ymin)/res
basemap<-raster( ,columns,rows,xmin,xmax,ymin,ymax, resolution = 0.01)
coord<-cellFromXY(basemap, cbind(monthlyDataFrame[,1], monthlyDataFrame[,2]))
basemap[coord]<-monthlyDataFrame[ ,3] 


pdf('testRaster.pdf') 
plot(basemap)
dev.off() 
pdf('testRasterPrec')
plot(rasterPrec)
dev.off()



