library(raster)
library(emast)

# createe test list
   long<-rep(1:20, each=20)
   lat<-rep(1:20,20)
   variable<-seq(1:400)
   baseLatLong<-data.frame(long,lat)
   dataFramePrec<-cbind(baseLatLong,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable)
   dataFrameRad<-cbind(baseLatLong,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable)
   dataFrameMin<-cbind(baseLatLong,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable)
   dataFrameMax<-cbind(baseLatLong,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable)
   dataFrameList<-list()

   #remove column names
   colnames(dataFramePrec)<-NULL
   colnames(dataFrameRad)<-NULL
   colnames(dataFrameMin)<-NULL
   colnames(dataFrameMax)<-NULL

   #add to list
   yearStart<-1970
   yearEnd<-1970
   range<-range(yearStart:yearEnd)

for(year in yearStart:yearEnd){
      #names for test data list
      namePrec<-paste0('prec_',year)
      nameRad<-paste0('rad_',year)
      nameMax<-paste0('max_',year)
      nameMin<-paste0('min_',year)

      # add test data frames to list iteratively 
      dataFrameList[[namePrec]]<-dataFramePrec
      dataFrameList[[nameRad]]<-dataFrameRad
      dataFrameList[[nameMax]]<-dataFrameMin
      dataFrameList[[nameMin]]<-dataFrameMax
      } 
 str(dataFrameList)

ANUOutput <- ANUClimate(dataFrameMin,dataFrameMax,dataFramePrec,dataFrameRad, output='dataframe')
str(ANUOutput)

