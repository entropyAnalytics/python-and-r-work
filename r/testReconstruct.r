library(raster)
x<-c(123,124,125,123,124,125,123,124,125)
y<-c(-43,-43,-43,-42,-42,-42,-41,-41,-41)
z<-c(1,2,3,4,5,6,7,8,9)


dataFrame<-data.frame(x,y,z)

print(head(dataFrame))

testRaster<-rasterFromXYZ(dataFrame)

print(testRaster)

dataFrame<-as.data.frame(testRaster)

print(head(dataFrame))()
