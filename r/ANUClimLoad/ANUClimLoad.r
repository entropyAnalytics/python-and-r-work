library(emast)
library(Rcpp)
library(sp)
library(raster)
#library(rgdal)
library(ncdf4)

## time variables
starting_year <-2002
ending_year<-2012
year_time_step <-1

#configuration variable for running model
# test mode
test<-FALSE
writeToFile<-TRUE
createRaster<-TRUE
createDirectories<-TRUE
irregularDataFrame<-FALSE
rowsToRemove<-33

# missing variable value
missingVariable<--9999

## vars for directories
precipitation_directory<-'/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'
radiation_directory<-'/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'
min_temp_directory<-'/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'
max_temp_directory<-'/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'
precipitationAcronym <-'prec'
radiationAcronym<-'rrad'
min_tempAcronym <-'tmin'
max_tempAcronym<-'tmax'
folderEnding<-'/e_01/1970_2012/'

##vars for file bases
precipitation_file_base<-'eMAST_ANUClimate_mon_prec_v1m0_'
radiation_file_base<-'eMAST_ANUClimate_mon_rrad_v1m0_'
min_temp_file_base<-'eMAST_ANUClimate_mon_tmin_v1m0_'
max_temp_file_base<-'eMAST_ANUClimate_mon_tmax_v1m0_'

#save to list
precList<-c(precipitation_directory,precipitation_file_base,precipitationAcronym,folderEnding)
radList<-c(radiation_directory,radiation_file_base,radiationAcronym,folderEnding)
minList<-c(max_temp_directory,min_temp_file_base,min_tempAcronym,folderEnding)
maxList<-c(max_temp_directory,max_temp_file_base,max_tempAcronym,folderEnding)

#variableList<-list(precList,radList,minList,maxList)

variableList<-list(precList,radList,minList,maxList)
print(variableList)
net_CDF_suffix<-'.nc'

## years / months sequences
years<-seq(starting_year,ending_year)
print(years)
monthsToRun<-12
months<-seq(1,monthsToRun)
print(months)
years_month_directory_base_list<-list()

#global data frames
dataFramePrec <-""
dataFrameRrad <-""
dataFrameTmax <-""
dataFrameTmin <-""
dataFrameAvg <-""
dataFrameList<-list()
tMinRasterObject<-""
tMaxRasterObject<-""
avgRasterObject<-""
tMinSet<-FALSE
tMaxSet<-FALSE
dataFrameMissing<-""


#function to find variable name based on 
findVariableName<-function(modelOutput){
  if (modelOutput=='BIO01'){
      return('tavg')
  }
    if (modelOutput=='BIO02'){
   return('dnrg')
 
  }

  if (modelOutput=='BIO03'){
 return('isot')

  }

if (modelOutput=='BIO04'){
 return('tsea')

  }


if (modelOutput=='BIO05'){
 return('mata')

  }

if (modelOutput=='BIO06'){
 return('miti')

  }

if (modelOutput=='BIO07'){
 return('trng')

  }

if (modelOutput=='BIO08'){
 return('twet')

  }

if (modelOutput=='BIO09'){
 return('tdry')

  }

if (modelOutput=='BIO10'){
 return('twrm')

  }

if (modelOutput=='BIO11'){
 return('tcmt')

  }

if (modelOutput=='BIO12'){
 return('pavg')

  }


if (modelOutput=='BIO13'){
 return('pwmt')

  }
if (modelOutput=='BIO14'){
 return('pdmt')

  }
if (modelOutput=='BIO15'){
 return('psea')

  }
if (modelOutput=='BIO16'){
 return('pwet')

  }
if (modelOutput=='BIO17'){
 return('pdry')

  }
if (modelOutput=='BIO18'){
 return('pwrm')

  }
if (modelOutput=='BIO19'){
 return('pcld')

  }
if (modelOutput=='BIO20'){
 return('arad')

  }
if (modelOutput=='BIO21'){
 return('hrad')

  }

if (modelOutput=='BIO22'){
 return('irad')

  }

if (modelOutput=='BIO23'){
 return('srad')

  }


if (modelOutput=='BIO24'){
 return('rwmt')

  }


if (modelOutput=='BIO25'){
 return('rdmt')

  }
if (modelOutput=='BIO26'){
 return('ramt')

  }
if (modelOutput=='BIO27'){
 return('rcmt')

  }
}

#function to generate missing data
generateMissingData<-function(){
#create raster file from input
rasterPrec<-raster('/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197001.nc')
print('This is the initial input data frame including missing values')
dataFrameMissing<-as.data.frame(rasterPrec, xy= TRUE, na.rm=FALSE)
colnames(dataFrameMissing)<-c('x','y','z')
print(str(dataFrameMissing))
print(head(dataFrameMissing))
print('This is the missing data from the input data frame')
dataFrameMissing<-subset(dataFrameMissing,is.na(dataFrameMissing[,3]))
print(str(dataFrameMissing))
print(head(dataFrameMissing))
return(dataFrameMissing)
#monthlyDataFrame<-rbind(monthlyDataFrame,dataFrameMissing)
}



#function to get yearly dataframes and save to file
getYearlyDataFrames<-function(yearlyDataFrame,year){
   startTime<-proc.time()
   numberOfColumns<-ncol(yearlyDataFrame)
   start<-3
   dataFrameBase<-yearlyDataFrame[ ,c(1,2)]
   dataFrameMissing<-generateMissingData()



#iterate monthly columns, make new dataFrames, create raster file, create directory and file name, write to file 
   for(var in start:numberOfColumns){
      annualDataFrame<-cbind(dataFrameBase,yearlyDataFrame[var])
      
      print(var)
      print(head(annualDataFrame))

      # this is where the missing data needs to be appended to the data frame
      print('This is the missing data from the input data frame')
      print(str(dataFrameMissing))
      print(head(dataFrameMissing))

      # variable from column header
      names<-colnames(annualDataFrame)
      print(names)
      variableName<-names[3]
      print(variableName)
      ##determine variable name
      variableName<-findVariableName(variableName)

      print(variableName)
      month<-""
      ##for the monthly data frame, get directory and filename 
      directoryAndFile<-generateDirectoryAndFileName(variableName,year,month)
      directoryName<-directoryAndFile$directory
      fileName<-directoryAndFile$file


      colnames(annualDataFrame)<-c('x','y','z')
      newAnnualDataFrame<-rbind(annualDataFrame,dataFrameMissing)
      print('This is the new montthly data frame after combined with missing data')
      print(str(newAnnualDataFrame))
      print(head(newAnnualDataFrame))
      colnames(newAnnualDataFrame)<-NULL

      
      #from monthly dataFrame save to raster object
      monthlyRasterObject<-convertToRaster(newAnnualDataFrame)


      ## for the monthly raster object save to file
      saveToFile(monthlyRasterObject,directoryName,fileName)
      totalTime<-proc.time()-startTime
     }
}


#function to get monthly data frames from yealy dataFrames, create raster files, create directory and file name, write to file
getMonthlyDataFrames<-function(yearlyDataFrame,variableName,year){
   startTime<-proc.time()
   numberOfColumns<-ncol(yearlyDataFrame)
   columns <-seq(3:numberOfColumns)
   dataFrameBase<-yearlyDataFrame[ ,c(1,2)]

#iterate monthly columns, make new dataFrames, create raster file, create directory and file name, write to file 
   for(month in columns){
      monthlyDataFrame<-cbind(dataFrameBase,yearlyDataFrame[,month])
      print(month)
      print(head(monthlyDataFrame))

      #from monthly dataFrame save to raster object
      monthlyRasterObject<-convertToRaster(monthlyDataFrame)

      ##for the monthly data frame, get directory and filename 
      directoryAndFile<-generateDirectoryAndFileName(variableName,year,month)
      directoryName<-directoryAndFile$directory
      fileName<-directoryAndFile$file

      ## for the monthly raster object save to file
      saveToFile(monthlyRasterObject,directoryName,fileName)
      totalTime<-proc.time()-startTime
     } 
}

#function to get non-monthly dataFrames, create raster files, create directory and filename, write to file
getNonMonthlyDataFrames<-function(multiVariableDataFrame,year){   
numberOfColumns<-ncol(multiVariableDataFrame)
numberOfColumns<-numberOfColumns-2
print(paste0('Number of Columns: ',numberOfColumns))
columns<-seq(1:numberOfColumns)
dataFrameBase<-multiVariableDataFrame[, c(1,2)]

##iterate through columns, get the name of each colum, make a new data frame, create raster file, create directory and file name, write to file
for(column in columns){
   column<-column+2
   print(paste0('Current Column: ',column))
   variableDataFrame<-cbind(dataFrameBase,multiVariableDataFrame[ , column])
columnNameToSave<-colnames(multiVariableDataFrame)[column]
columnNameToSave<-paste0('_',columnNameToSave)
print(columnNameToSave)
print(head(variableDataFrame))

# save to raster object
if(createRaster==TRUE){
variableRasterObject<-convertToRaster(variableDataFrame)
}

if(createRaster==FALSE){
print('Set createRaster config to TRUE to create raster')
}

##for the variable data frame, get directory and filename 
directoryAndFile<-generateDirectoryAndFileName(variableName,year,columnNameToSave)
directoryName<-directoryAndFile$directory
fileName<-directoryAndFile$file
## for the monthly raster object save to file
saveToFile(variableRasterObject,directoryName,fileName)
}
}


#function to generate name directory path and name of file
# "temp/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197001.nc"
generateDirectoryAndFileName<-function(variableAcronym,year,month){
print('generating directory and filename')
baseDirectory<-'g/data/rr9/temp40yearsANU/'
directoryStructure1<-'Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/yr/land/'
short<-paste0(variableAcronym,'/')
directoryStructure2<-'e_01/1970_2012/'
fileBase1<-'eMAST_ANUClimate_yr_'
fileBase2<-'_v1m0_'
fileSuffix<-'_v0.nc'


##append 0 to month if 1-9

# create directory and file name
directoryName<-paste0('/',baseDirectory,directoryStructure1,short,directoryStructure2)
fileName<-paste0(fileBase1,variableAcronym,fileBase2,year,month,fileSuffix)

namesList<-list(directory=directoryName,file=fileName)
print(namesList$directory)
print(namesList$file)
return(namesList)
}

#function to convert to raster object
convertToRaster<-function(monthlyDataFrame){
   startTime<-proc.time()
   print('Converting to monthly dataframe to monthly raster')

   if(createRaster==FALSE){
      print('To enable creating raster object, change config createRaster to TRUE')
   } 
   if(createRaster==TRUE){
   # new way of creating raster file with rasterFromXYZ

   if(irregularDataFrame==TRUE){
     print('removing rows to make irregular')
     rows<-nrow(monthlyDataFrame)
     print(rows)
     monthlyDataFrame<-monthlyDataFrame[-sample(rows,rowsToRemove), ]
     print(paste0('New Rows: ',nrow(monthlyDataFrame)))
   }

#previous way of making raster file, could not account for missing data
#rasterObject<-rasterFromXYZ(monthlyDataFrame)
#return(rasterObject)

   xmin<-112.9
   xmax<-154
   ymin<--43.74
   ymax<--9
   res<-0.01
   columns<-4110
   rows<-3474
   print('creating base layer')
   basemap<-raster( ,columns,rows,xmin,xmax,ymin,ymax,resolution=0.01)
   coord<-cellFromXY(basemap, cbind(monthlyDataFrame[,1], monthlyDataFrame[,2]))
   print(basemap)
   basemap[coord]<-monthlyDataFrame[ ,3]
   print("This is the raster object after the values are added") 
   print(basemap)
   totalTime<-proc.time()-startTime
   print(paste0('Total time to create raster file',totalTime))
   return(basemap)
   }
}  

#function to save to save to file
saveToFile<-function(rasterFile,directory,file){
    completeFilePath<-paste0(directory,file)
    
    #check to see if directory exists, if it does not, then create it
    # dir.create() will show a warning if directory alreday exists but not an error
    print(paste0('Creating directory: ',directory))
    if(createDirectories==FALSE){
      print('set createDirectories to TRUE to create directories')
    }
    
    if(createDirectories==TRUE){
       dir.create(directory,recursive=TRUE)   
     }
    
    #check to see if file exists, if it does not, write write file
    if(file.exists(completeFilePath)){
       print(paste(file,' exists but is being removed'))
       file.remove(completeFilePath)
    }
    
    if(writeToFile==FALSE){
    print(paste0('Raster file will be written to: ',directory,file,' when write config enabled'))
    }

    if(writeToFile==TRUE){
    print(paste0('Writing raster file to: ',directory,file))
    writeRaster(rasterFile,completeFilePath)
    }
}


# function to run model

runModel<-function(dataFrameList){
#global variables for dataFrames
dataFramePrec<-""
dataFrameRrad<-""
dataFrameTmin<-""
dataFrameTmax<-""

#global list to store dataframes
dataFrameNames<-names(dataFrameList)

#when variable counter gets to four, the model will be run
variableCounter<-0



for (dataFrameName in dataFrameNames){
   variableCounter<-variableCounter+1
   yearAndName<-strsplit(dataFrameName,"_")
   print(yearAndName)
   variableName<-yearAndName[[1]][1]
   year<-yearAndName[[1]][2]
	
   if(variableName=='prec'){
      dataFramePrec<-dataFrameList[[dataFrameName]]
      print(paste(variableCounter,variableName))
      print(head(dataFramePrec))
      }
   
   if(variableName=='rad'){
      dataFrameRrad<-dataFrameList[[dataFrameName]]
      print(paste(variableCounter,variableName))
 
      print(head(dataFrameRrad))
      }

   if(variableName=='tmax'){
      dataFrameTmax<-dataFrameList[[dataFrameName]]
      print(paste(variableCounter,variableName))
      print(head(dataFrameTmax))
      }

   if(variableName=='tmin'){
      dataFrameTmin<-dataFrameList[[dataFrameName]]
      print(paste(variableCounter,variableName))
      print(head(dataFrameTmin))
   }
   
   if (variableCounter==4){
   print("Will run model here after dataFrames are obtained for each year")
   output<-ANUClimate(dataFrameTmin,dataFrameTmax,dataFramePrec,dataFrameRrad,output='dataframe')
   variableCounter=0
   print(str(output))
   newVariableNames<-names(output)
   
   #for each new variable produced by the model, split into monthly data frames, generate directory and filename, convert to raster, save to file
  
   for (newVariableName in newVariableNames){

     #total is not a requierd data product
     #if(newVariableName =='Total'){
     #   print('Saving Total')
     #   getNonMonthlyDataFrames(output[[newVariableName]],'totl',year)
     #}

     if(newVariableName =='Temp'){
        print('Saving Temp Annual variables')
        getYearlyDataFrames(output[[newVariableName]],year)
     }

     if(newVariableName =='Prec'){
        print('Saving Prec annual variables')
        getYearlyDataFrames(output[[newVariableName]],year) 
     }

     if(newVariableName =='Rad'){     
        print('Saving Rad annual variables')
        getYearlyDataFrames(output[[newVariableName]],year)
     }
   }
}

}
} # end runModel function


## function that takes directory string builds initial dataframe
makeDataFrames<-function(filePath){
  startTime<-proc.time()
  rasterObject<-raster(filePath)
  print(paste('makingRasterFile',filePath,sep=""))
  variableDataFrame<-rasterToPoints(rasterObject)
  colnames(variableDataFrame)<-NULL
  print(paste('converting to dataFrame',filePath,sep=""))
  totalTime<-proc.time()-startTime
  print(paste0('Time taken to create data frame: ',totalTime))
  return (variableDataFrame)
}

## function that creates data frame but strips out the variable vector only to append to a data frame for months 2-12
appendDataFrames<-function(filePath){
  startTime<-proc.time()
  rasterObject<-raster(filePath)
  print(paste('appending raster file',filePath,sep=""))
  variableDataFrame<-rasterToPoints(rasterObject)
  print(paste('converting to dataFrame',filePath,sep=""))
  print('slicing variable column')
  variableColumn<-variableDataFrame[,3]
  print(head(variableColumn))
  totalTime<-proc.time()-startTime
  print(paste0('Time taken to append data frame',totalTime))
  return (variableColumn)
}

makeDataFrameFromRaster<-function(rasterObject){
variableDataFrame<-rasterToPoints(rasterObject)
colnames(variableDataFrame)<-NULL
print('Converting raster file to data frame')
return(variableDataFrame)
}

appendDataFrameFromRaster<-function(rasterFile){
  print('Converting to dataFrame')
  print('slicing variable column')
  variableDataFrame<-rasterToPoints(rasterFile)
  variableColumn<-variableDataFrame[,3]
  print(head(variableColumn))
  return (variableColumn)
}


#function to create average data frame from min and max, converts each to raster then appends data frame
createAverageDataFrame<-function(minDataFrame,maxDataFrame){
startTime<-proc.time()
dataFrame<-minDataFrame[ ,c(1,2)]
columnsInDataFrame<-ncol(minDataFrame)
print(columnsInDataFrame)
columns<-columnsInDataFrame-2
print(columns)
for(column in 1:columns){
   column<-column+2
   print(paste0('Creating min raster :',column))
   rasterMin<-rasterFromXYZ(minDataFrame[,c(1,2,column)])
   print(head(minDataFrame[,c(1,2,column)]))
   print(rasterMin)
   print(paste0('Creating max raster :',column))
   rasterMax<-rasterFromXYZ(maxDataFrame[,c(1,2,column)])
   print(rasterMax)
   print('Calculating Average')
   rasterAverage<-(rasterMin+rasterMax)/2
   print('rasterAverage')
   averageDataFrame<-rasterToPoints(rasterAverage)
   print(head(averageDataFrame))
   dataFrame<-cbind(dataFrame,averageDataFrame[,3])
   }
totalTime<-proc.time()-startTime
print(paste0('Total Time to create average data frame: ',totalTime))
return(dataFrame)
}


## iterate over the year / month / directory / base to produce file name
#year time
yearTimeStart<-proc.time()
print('Starting load:')    


if (test==TRUE){

   long<-rep(1:20, each=20)
   lat<-rep(-11:-30,20)
   variable<-seq(1:400)
   baseLatLong<-data.frame(long,lat)
   dataFramePrec<-cbind(baseLatLong,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable)
   dataFrameRad<-cbind(baseLatLong,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable)
   dataFrameMin<-cbind(baseLatLong,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable)
   dataFrameMax<-cbind(baseLatLong,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable,variable)
   dataFrameList<-list()

   #remove column names
   colnames(dataFramePrec)<-NULL
   colnames(dataFrameRad)<-NULL
   colnames(dataFrameMin)<-NULL
   colnames(dataFrameMax)<-NULL

   #add to list
   yearStart<-1970
   yearEnd<-1970
   range<-range(yearStart:yearEnd)

for(year in yearStart:yearEnd){
      #names for test data list
      precipListIndexName<-paste0('prec_',year)
      rradListIndexName<-paste0('rad_',year)
      TMaxIndexName<-paste0('tmax_',year)
      TMinIndexName<-paste0('tmin_',year)


      dataFrameList[[precipListIndexName]]<-dataFramePrec
      dataFrameList[[rradListIndexName]]<-dataFrameRad
      dataFrameList[[TMinIndexName]]<-dataFrameMax
      dataFrameList[[TMaxIndexName]]<-dataFrameMin


      }
str(dataFrameList)
print('running Model with test data')
runModel(dataFrameList)

}# end test data generation


if (test==FALSE){

for(year in years){ 
    for (list in variableList){
        for(month in months){ 
           if(month<10){
              fileName<-paste(list[1],list[3],list[4],list[2],year,'0',month,net_CDF_suffix,sep="")
           }
           if(month>=10){
              fileName<-paste(list[1],list[3],list[4],list[2],year,month,net_CDF_suffix,sep="")
           }
           print(fileName) 
       
       #create dataFrame based on directory
        if(month ==1){
           dataFrame1<-makeDataFrames(fileName)
           print(head(dataFrame1))
        }
           
        if(month >1){
           dataFrame1<-cbind(dataFrame1,appendDataFrames(fileName))
           print(head(dataFrame1))
         }     
         print(list[3])   
          
         if(identical(list[3],'prec')){
            dataFramePrec<-dataFrame1
         }  

         if(identical(list[3],'rrad')){
            dataFrameRrad<-dataFrame1
	 
         } 
         
         if(identical(list[3],'tmin')){
            dataFrameTmin<-dataFrame1   
         }

         if(identical(list[3],'tmax')){
            dataFrameTmax<-dataFrame1 
                  
    
           if(month==monthsToRun){
      
             precipListIndexName<-paste0('prec_',year)
	     rradListIndexName<-paste0('rad_',year)
	     TMaxIndexName<-paste0('tmax_',year)
	     TMinIndexName<-paste0('tmin_',year)
          
	     dataFrameList[[precipListIndexName]]<-dataFramePrec
	     dataFrameList[[rradListIndexName]]<-dataFrameRrad
             dataFrameList[[TMinIndexName]]<-dataFrameTmin
             dataFrameList[[TMaxIndexName]]<-dataFrameTmax
	     print(str(dataFrameList))
             
             #run the dataFrame List through the model
             print('running Model')
             runModel(dataFrameList)
             
             #remove the dataFrame List from memory
	     print('clearing dataframe list')
             dataFrameList[[precipListIndexName]]<-NULL
	     dataFrameList[[rradListIndexName]]<-NULL
             dataFrameList[[TMaxIndexName]]<-NULL
             dataFrameList[[TMinIndexName]]<-NULL
             
             } # final month if
         }# end if for tmax, the last variable
      }# end month for
   } # end variable for
 } # end year loop 
} # end if statement for non-test data
timeTaken<-proc.time()-yearTimeStart
print(paste('Time taken:',timeTaken))



